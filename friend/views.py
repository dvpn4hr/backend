from django.shortcuts import render
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.db import transaction
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from .models import User, UserProfile
import json
import datetime
from django.utils.timezone import utc

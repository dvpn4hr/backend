from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
import uuid


# Create your models here.
class FriendManager(models.Manager):
    def create_friend(
                      self,
                      email,
                      telegram_handle,
                      has_connected,
                      passcode,
                      cert_id,
                      language
                    ):
        """Creates a new friend with provided email"""
        friend = Friend.objects.create(email=email)
        return friend

class EmailLowerField(models.EmailField):
    def to_python(self, value):
        if value: # if value is not Null
            #change email address to lower case
            return value.lower()
        else:
            return value

class Friend(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
                            'user.User',
                            related_name='friend',
                            on_delete=models.CASCADE,
                            null=True, blank=True
                            )
    unique_id = models.UUIDField(default=uuid.uuid4, unique=True)
    email = EmailLowerField(max_length=255, null=True)
    name = models.CharField(max_length=200, null=True, blank=True, default="NA")
    telegram_handle = models.CharField(max_length=200, null=True, blank=True)
    has_connected = models.BooleanField(default=False)
    subscribed = models.BooleanField(default=True)
    usage_status = models.IntegerField(default=0)
    passcode = models.CharField(max_length=200, null=True, blank=True)
    cert_id = models.CharField(max_length=20, null=False, blank=False, default="xx.xx")
    cert_hash = models.CharField(max_length=30, null=True, blank=True)
    language = models.CharField(max_length=3, null=True, blank=True)
    config = models.JSONField(null=True, blank=True)
    objects = FriendManager()

    def __str__(self):
        if self.email: # if email is not None
            return self.email
        elif self.name: # if name is not None
            return self.name
        else: # if email and name are both None
            return self.cert_id


    class Meta:
        constraints = [
        models.UniqueConstraint(fields = ['user','email'], name='unique_email'),
        models.UniqueConstraint(fields = ['user', 'name'], name='unique_name'),
        models.UniqueConstraint(fields = ['user', 'cert_id'], name='unique_cert_id')
        ]

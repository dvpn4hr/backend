from rest_framework import viewsets, status
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from rest_framework.response import Response
from django.core.exceptions import ValidationError
from rest_framework.decorators import api_view, action
from django.db import IntegrityError


from .models import Friend
from device.models import Device
from .serializers import FriendSerializer
from actions import actions
from .permissions import IsSame
from time import sleep


class FriendViewSet(viewsets.ModelViewSet):
    queryset = Friend.objects.all()
    serializer_class = FriendSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of
        permissions that this view requires.
        """
        print(self.action)
        if self.action in ['list', 'retrieve', 'update', 'partial_update']:
            # Only allow same user to view/edit/update its own record
            print('execution reaches here')
            permission_classes = [IsAuthenticated, IsSame]  # , IsSame]
        elif self.action == 'create':
            # allow anyone to create (sign up) a new user
            permission_classes = [IsAuthenticated]
        elif self.action == 'destroy':
            permission_classes = [IsAuthenticated, IsSame]
        elif self.action in ['resend']:
            # print('setting up permissions for heartbeat')
            permission_classes = [IsAuthenticated, IsSame]
        elif self.action == 'subscribe':
            # allow anyone to create (sign up) a new user
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        print("creating the serializer")
        serializer = self.get_serializer(data=request.data)
        # serializer.user = request.user
        try:
            serializer.is_valid(raise_exception=True)
            print(request.data)
            serializer.save(user=request.user)
        except IntegrityError as e:
            print(" Error:" + str(e))
            content = {'error_description': 'Friend already exists'}
            return Response(content, status=status.HTTP_409_CONFLICT)
        else:
            # print(serializer.data)
            headers = self.get_success_headers(serializer.data)
            return Response(
                            serializer.data,
                            status=status.HTTP_201_CREATED,
                            headers=headers
                            )

    def list(self, request):
        queryset = Friend.objects.filter(user=request.user)
        serializer = FriendSerializer(queryset, many=True)
        return Response(serializer.data)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='resend')
    def resend(self, request, pk=None):
        friend = self.get_object()
        self.check_object_permissions(request, friend)
        print(friend.user)
        print("Resend email with access key to friend...")
        actioninstance = actions.Actions()
        queryset = Device.objects.filter(user=request.user)
        for device in queryset:
            msg = { 'action': "add_user",
            		'cert_name': friend.cert_id,
            		'email': friend.email,
            		'passcode': friend.passcode,
            		'language': friend.language,
                    'uuid': str(friend.unique_id),
                    'subscribed': friend.subscribed,
                    'id':friend.id,
                    'config': friend.config,
            		}
            actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        content = {'Successful': 'email is resent'}
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='subscribe')
    def subscribe(self, request, pk=None):
        """
        The function of this endpoint to process a link in the form of
        `api/friend/{id}/subscribe/?uuid=UNIQUE_ID&subscribe=false`.
        This link will be embedded in any email sent to friends in order
        to give them an option to opt in/out of receving email notifications
        in the future. Each uuid is tied to a unique friend resource in
        the database.

        If `subscribe` parameter is set to `false`, the friend will stop
        receiving email notification, and if set to `true`, continue to
        receives them.
        """
        # TODO: add subscribed model field to friend models
        # include subscribe param in GET header
        # set params accordingly
        friend = self.get_object()
        # self.check_object_permissions(request, friend)
        # print(friend.user)
        print(friend.unique_id)
        print(request.GET.get('uuid'))
        print(request.GET.get('subscribe'))
        if (str(friend.unique_id) == str(request.GET.get('uuid'))):
            if str(request.GET.get('subscribe'))=='false':
                print("Unsubscribing friend from email ...")
                friend.subscribed = False
                friend.save()
                content = 'You have successfully opted out of receiving email' \
                        ' notifications. To subscribe again, click on the' \
                        ' subscribe link on the footer of the last email ' \
                        ' you received or ask your provider.'
            elif str(request.GET.get('subscribe'))=='true':
                print("Subscribing back ...")
                friend.subscribed = True
                friend.save()
                content = 'You have successfully subscribed back to receive' \
                            ' email notifications'
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            #actioninstance = actions.Actions()
            #queryset = Device.objects.filter(user=friend.user)
            #for device in queryset:
            #    actioninstance.publish_mqtt_command(device.id, "add_user",
            #                    friend.cert_id, friend.email, friend.passcode,
            #                    friend.subscribed, friend.unique_id, friend.id)
            #actioninstance.disconnect()
            return Response(content, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Friend


class FriendSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(allow_null=True, required=False, read_only=True)
    email = serializers.EmailField(allow_null=True, required=False, default=None)
    name = serializers.CharField(allow_null=True, required=False,default=None)
    telegram_handle = serializers.CharField(required=False, allow_null=True, default=None)
    has_connected = serializers.NullBooleanField(required=False, default=False)
    usage_status = serializers.IntegerField(required=False)
    passcode = serializers.CharField(required=False, default='')
    cert_id = serializers.CharField(allow_null=True, required=False, default=None)
    language = serializers.CharField(allow_null=True, default='en')
    config = serializers.JSONField(required=False, default={})
    subscribed = serializers.NullBooleanField(required=False, default=True)

    class Meta:
        model = Friend
        fields = (
            'id',
            'email',
            'telegram_handle',
            'has_connected',
            'usage_status',
            'passcode',
            'cert_id',
            'language',
            'name',
            'config',
            'subscribed'
        )

    def validate(self, data):
        """
        Check if passphrase is provided if email address is given.
        """
        if (data.get('email') is not None) and (data.get('passcode') is None):
            raise serializers.ValidationError('passcode required when email is provided.')
        return data

    def create(self, validated_data):
        print(validated_data)
        f = Friend.objects.create(
                        user=validated_data['user'],
                        email=validated_data['email'],
                        name=validated_data['name'],
                        telegram_handle=validated_data['telegram_handle'],
                        has_connected=validated_data['has_connected'],
                        passcode=validated_data['passcode'],
                        cert_id=validated_data['cert_id'],
                        language=validated_data['language'],
                        config=validated_data['config'],
                        subscribed=validated_data['subscribed'],
        )
        return f

#    def save(self):
#        email = self.validated_data['email']
#        message = 'message'
#        #send_email(from=email, message=message)

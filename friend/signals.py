from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver
from user.models import User
from device.models import Device
from .models import Friend
from actions import actions
import random, string


@receiver(pre_save, sender=Friend)
def check_null_fields(sender, instance, **kwargs):
    # If instance is being created for the first time, make sure
    # name field will be set if left emty with email address
    # if no email address is specified use cert ID as name
    if instance.id is None:
        # generate cert_id if not provided by API
        if (instance.cert_id is None):
            x = ''.join(random.choices(string.ascii_letters + string.digits, k=2))
            y = ''.join(random.choices(string.ascii_letters + string.digits, k=2))
            # print(x + "." + y)
            instance.cert_id = x + "." + y
        # set name if not provided with email or cert_id
        if (instance.name is None) and (instance.email is not None):
            instance.name = instance.email
        if (instance.name is None) and (instance.email is None):
            instance.name = instance.cert_id


# if there's any change in friends object, this method will
# notify the device via MQTT action
@receiver(pre_save, sender=Friend)
def notify_device(sender, instance, **kwargs):
    # If instance is NOT being created for the first time
    if instance.id is not None:
        # if the instance being update, notify the device of changes
        current = instance
        previous = Friend.objects.get(id=instance.id)
        print('save action happening on friend instance...')
        # if there's a change in any of the following params,
        # initiate an update process via MQTT
        if (current.subscribed != previous.subscribed) or \
            (current.email != previous.email) or \
            (current.cert_id != previous.cert_id) or \
            (current.language != previous.language) or \
            (current.passcode != previous.passcode) or \
            (current.config != previous.config):
            #initiate MQTT action
            print('change in friend fields detected....')
            try:
                actioninstance = actions.Actions()
                queryset = Device.objects.filter(user=current.user)
                msg = { 'action': "add_user",
                		'cert_name': current.cert_id,
                		'email': current.email,
                		'passcode': current.passcode,
                		'language': current.language,
                        'uuid': str(current.unique_id),
                        'subscribed': current.subscribed,
                        'id':current.id,
                        'config':current.config
                		}
                for device in queryset:
                    print('sending mqtt action to device' + str(device.id))
                    actioninstance.publish_mqtt_msg(device.id, msg)
                actioninstance.disconnect()
            except Exception as e:
                print(" Error" + str(e))

# this method will notify the device when a friend if added for the first time
@receiver(post_save, sender=Friend)
def notify_device_add(sender, instance, created, **kwargs):
    if created:
        queryset = Device.objects.filter(user=instance.user)
        print("sending mqtt command to create certificate on device")
        try:
            if queryset.exists():
                actioninstance = actions.Actions()
                for device in queryset:
                    print("sending MQTT command to device " + str(device.id))
                    msg = { 'action': "add_user",
                    		'cert_name': instance.cert_id,
                    		'email': instance.email,
                    		'passcode': instance.passcode,
                    		'language': instance.language,
                            'uuid': str(instance.unique_id),
                            'subscribed': instance.subscribed,
                            'id': instance.id,
                            'config':instance.config
                    		}
                    actioninstance.publish_mqtt_msg(device.id, msg)
                actioninstance.disconnect()
            else:
                print("No devices found for user = " + str(instance.user.email))
        except Exception as e:
            print(" Error" + str(e))
            raise e

# this method will notify the device when a friend is deleted
@receiver(post_delete, sender=Friend)
def notify_device_delete(sender, instance, **kwargs):
    queryset = Device.objects.filter(user=instance.user)
    print("sending mqtt command to delete friend " + \
            str(instance.email) + \
            " certificates on device")
    try:
        if queryset.exists():
            actioninstance = actions.Actions()
            for device in queryset:
                print("sending delete MQTT command to device " + str(device.id))
                msg = { 'action': "delete_user",
                        'cert_name': instance.cert_id,
                        'email': instance.email,
                        'passcode': instance.passcode,
                        'language': instance.language,
                        'uuid': str(instance.unique_id),
                        'subscribed': instance.subscribed,
                        'id': instance.id,
                        'config':instance.config
                        }
                actioninstance.publish_mqtt_msg(device.id, msg)
            actioninstance.disconnect()
        else:
            print("The user " + \
                str(instance.user.email) + \
                " for friend " + str(instance.email) + \
                " has no devices claimed")
    except Exception as e:
        print(" Error" + str(e))
        raise e

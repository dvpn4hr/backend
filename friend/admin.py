from django.contrib import admin
from .models import Friend


class FriendAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'email', 'name')
    search_fields = ['email', 'user__email']

# Register your models here.
admin.site.register(Friend, FriendAdmin)

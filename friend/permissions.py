from rest_framework.permissions import BasePermission
from .models import Friend


class IsSame(BasePermission):
    """Custom permission class to allow only owners to edit them."""

    def has_object_permission(self, request, view, obj):
        """Return True if permission is granted to the friend owner."""
        print("request.user.email = " + request.user.email)
        print("obj.email = " + obj.user.email)
        if isinstance(obj, Friend):
            return obj.user == request.user
        # print("request.user.email = " + request.user.email)
        # print("obj.email = " + obj.email)
        return obj.user == request.user

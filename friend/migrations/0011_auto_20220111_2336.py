# Generated by Django 3.1.13 on 2022-01-11 23:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friend', '0010_auto_20220111_2007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='cert_id',
            field=models.CharField(default='xx.xx', max_length=20),
        ),
        migrations.AddConstraint(
            model_name='friend',
            constraint=models.UniqueConstraint(fields=('user', 'cert_id'), name='unique_cert_id'),
        ),
    ]

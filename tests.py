import requests
import json
import string
import random
from datetime import datetime, timezone
from decouple import AutoConfig
import os
import pytest
import time

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_PATH = os.path.join(BASE_DIR, 'pp_backend/')
config = AutoConfig(search_path=CONFIG_PATH)

env = "dev"
if env == "prod":
    base_url = 'https://api.we-pn.com/'
    client_id = config('production_client_id')
    client_secret = config('production_client_secret')
    admin_username = config('admin_username')
    admin_password = config('admin_password_prod')
if env == "dev":
    base_url = 'https://api-dev.we-pn.com/'
    client_id = config('dev_client_id')
    client_secret = config('dev_client_secret')
    admin_username = config('admin_username')
    admin_password = config('admin_password_dev')

# ####################################################################
admin_auth_token = "#nosec:JUSTAPLACEHOLDER" #nosec: not a real token
user1_auth_token = "#nosec:JUSTAPLACEHOLDER" #nosec: not a real token
user2_auth_token = "#nosec:JUSTAPLACEHOLDER" #nosec: not a real token

user1_id = None
user2_id = None
victim_user_id = None

friend1_id = None
friend2_id = None

device1_id = None
device2_id = None

message1_id = None
experiment1_id = None
experiment2_id = None


device1_serial_number = '111222'
device1_device_key = '47TDQ9HBBFY'

device2_serial_number = '222333'
device2_device_key = 'YVTBT9NEY75'

# create user 1 with random user name
name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
username1 = name + '@gmail.com'
password1 = 'powerfull12345'

# create test user 2 without a name
name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
username2 = name + '@gmail.com'
password2 = 'springtime56487'

# update user 2 email address
name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
new_email_address = name + '@gmail.com'

# =====================HTML output ========================

# making HTML output pretty
@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    #extra.append(pytest_html.extras.url('https://www.we-pn.com/'))
    #extra.append(pytest_html.extras.image('https://we-pn.com/img/logo.png'))
    if report.when == 'call':
        # always add url to report
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            # only add additional html on failure
            extra.append(pytest_html.extras.html('<div>Failed Instance</div>'))
    report.extra = extra


# #####################################################################
@pytest.mark.dependency()
def test_create_user1():
    print("creating user with username: " + username1)
    # create user object
    # usr1 = User(baseurl, user_name, password, client_id, client_secret)
    global user1_id
    api_url = 'api/user/'
    url = base_url + api_url
    payload = {
            'email': username1,
            'firstname': 'testUser 1',
            'lastname': 'testy',
            'password': password1,
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    #user1_auth_token = jresponse['access_token']
    user1_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass


#@pytest.mark.dependency(depends=["test_admin_login"])
@pytest.mark.dependency()
def test_create_user2():
    print("creating user with username: " + username2)
    # create user object
    # usr1 = User(baseurl, user_name, password, client_id, client_secret)
    global user2_id
    api_url = 'api/user/'
    url = base_url + api_url
    payload = {
            'email': username2,
            'password': password2,
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    #user2_auth_token = jresponse['access_token']
    user2_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

#@pytest.mark.dependency(depends=["test_admin_login"])
@pytest.mark.dependency()
def test_create_user_with_exisitng_email():
    print("creating user with username: " + username2)
    # create user account with an email that already exists in system
    # global user2_id
    api_url = 'api/user/'
    url = base_url + api_url
    payload = {
            'email': username2,
            'password': password2,
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    #jresponse = response.json()
    #print(response.status_code)
    print(response.text)
    #print(jresponse['access_token'])
    #user2_auth_token = jresponse['access_token']
    # user2_id = jresponse['id']
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass


###########################################################
@pytest.mark.dependency()
def test_admin_login():
    global admin_auth_token
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":admin_username,
            "password":admin_password,
            "client_id":client_id,
            "client_secret":client_secret
            }

    headers = {
            "content-type": "application/json"
            }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    print(response.status_code)
    print(response.text)
    #print(jresponse)
    #print(jresponse['access_token'])
    admin_auth_token = jresponse['access_token']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_create_user1"])
def test_login_user1():
    global user1_auth_token
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":username1,
            "password":password1,
            "client_id":client_id,
            "client_secret":client_secret}

    headers = {
            "content-type": "application/json"
            }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    #print(jresponse)
    #print(jresponse['access_token'])
    print(response.status_code)
    print(response.text)
    user1_auth_token = jresponse['access_token']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_create_user2"])
def test_login_user2():
    global user2_auth_token
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":username2,
            "password":password2,
            "client_id":client_id,
            "client_secret":client_secret}

    headers = {
            "content-type": "application/json"
            }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    #print(jresponse)
    #print(jresponse['access_token'])
    print(response.status_code)
    print(response.text)
    user2_auth_token = jresponse['access_token']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_login_user1"])
def test_login_fail():
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":username1,
            "password":"clearlywrong",
            "client_id":client_id,
            "client_secret":client_secret}

    headers = {
            "content-type": "application/json"
            }
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    assert(response.status_code != 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_login_user2"])
def test_partial_update_user2_email():
    print("updating user email address "+ username2 + " with :" + new_email_address)
    # create user object
    # usr1 = User(baseurl, user_name, password, client_id, client_secret)
    # global user2_id
    api_url = 'api/user/' + str(user2_id) + '/'
    url = base_url + api_url
    payload = {
            'email': new_email_address,
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    #print(payload)
    response = requests.patch(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    #user2_auth_token = jresponse['access_token']
    # user2_id = jresponse['id']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_create_user2"])
def test_relogin_user2():
    global user2_auth_token
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":new_email_address,
            "password":password2,
            "client_id":client_id,
            "client_secret":client_secret}

    headers = {
            "content-type": "application/json"
            }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    user2_auth_token = jresponse['access_token']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass


# ##################test GET user#####################################
@pytest.mark.dependency(depends=["test_login_user1"])
def test_get_user1():
    # query user1
    api_url = 'api/user/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert((response.status_code == 200) and (user1_id == jresponse[0]["id"]) ) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_login_user2"])
def test_get_user2():
    # query user2
    api_url = 'api/user/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert((response.status_code == 200) and (user2_id == jresponse[0]["id"]) ) #nosec: assert is a legit check for pytest
    pass

# ######################Test Create Device###############################

# a) create new device (try with user 1 who is not admin)
# Regular user should not be able to create new device

@pytest.mark.dependency(depends=["test_create_user1"])
def test_create_device_non_admin():
    api_url = 'api/device/'
    url = base_url + api_url
    payload = {
                'name': 'testDevice',
                'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'pin': '123',
                'port': '25',
                'status': '1',
                'ip_address': '1.1.1.1',
                'public_key': 'AAAAB3NzaC1yc2EAAA',
                'local_token': 'xyz',
                'local_ip_address': '0.0.0.0',
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    assert(response.status_code != 201) #nosec: assert is a legit check for pytest
    pass

# b) create new device (try with admin user)

@pytest.mark.dependency(depends=["test_admin_login"])
def test_create_device1_by_admin():
    global device1_id
    api_url = 'api/device/'
    url = base_url + api_url
    payload = {
                'name': 'testDevice1',
                'serial_number': device1_serial_number,
                'device_key': device1_device_key,
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.post(url, json=payload, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    device1_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_admin_login"])
def test_create_device2_by_admin():
    global device2_id
    api_url = 'api/device/'
    url = base_url + api_url
    payload = {
                'name': 'testDevice2',
                'serial_number': device2_serial_number,
                'device_key': device2_device_key,
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.post(url, json=payload, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    device2_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

'''
@pytest.mark.dependency(depends=["test_admin_login"])
def test_duplicate_device_serial_number():
    # must not be able to create device with duplicate
    # serial number
    api_url = 'api/device/'
    url = base_url + api_url
    payload = {
                'name': 'testDevice_duplicate',
                'serial_number': device1_serial_number,
                'device_key': device1_device_key,
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.post(url, json=payload, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code != 201) #nosec: assert is a legit check for pytest
    device1_id = jresponse['id']
    pass
'''

#============ Claim related tests =========================

# try adding a friend before claiming a device by user2
@pytest.mark.dependency(depends=[
                                "test_login_user2",
                                ])
def test_add_friend2():
    # delete user1
    # api_url = 'api/frined/' + str(device1_id) + '/'
    global friend2_id
    api_url = 'api/friend/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            "email": "TestFriend2@we-pn.com",
            # "telegram_handle": "@inja",
            #"has_connected": "False",
            "passcode": "test",
            #"cert_id": "wp.23",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    friend2_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

# test fetching a device before claiming it
# this operation is prohibited
@pytest.mark.dependency(depends=[
                        "test_login_user1",
                        "test_create_device1_by_admin"
                        ])
def test_fetch_unclaimed_device():
    # get a list of all CLAIMED devices (api/device/) bu user1
    # at this point there are no devices returned
    # since user1 has not yet claimed any device
    api_url = 'api/device/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and (len(jresponse)==0) ) #nosec: assert is a legit check for pytest
    pass

# test updating a device info before claiming it
# this operation is prohibited
@pytest.mark.dependency(depends=[
                        "test_login_user1",
                        "test_create_device1_by_admin"
                        ])
def test_update_unclaimed_device():
    # attempt to update info of an unclaimed device
    # this action is not permitted and must return Forbidden Error
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url
    # update device with following default parameters
    payload = {
            'name': 'testDevice_updated',
            'serial_number': '111111',
            'device_key': device1_device_key,
            'pin': '123',
            'port': '25',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.4',
            'public_key': 'AAAAB3NzaC1yc2EAAA',
            'local_ip_address':'1.2.3.4',
            'local_token':'sbsee'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.put(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

# @pytest.mark.dependency(depends=[
#                         "test_login_user1",
#                         "test_create_device_by_admin"
#                         ])
# def test_partial_update_unclaimed_device():

# this operation is prohibited
@pytest.mark.dependency(depends=[
                "test_create_user1",
                "test_create_device1_by_admin"
                ])
def test_claim_device_wrong_key():
    # try to claim a device that is already claimed
    # by user 1 with another user (user 2)
    # this action must be prevented.
    api_url = 'api/device/claim/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            'device_key': 'wrongkey',
            'device_name': "testDeviceName"
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    #print(response.status_code)
    #print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                "test_create_device1_by_admin"
                ])
def test_is_claimed_device1_before_being_claimed():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/is_claimed/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                "test_create_device1_by_admin"
                ])
def test_is_claimed_wrong_serial_number():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/is_claimed/'
    url = base_url + api_url
    payload = {
            'serial_number': 'randomserial',
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                "test_login_user1",
                "test_create_device1_by_admin"
                ])
def test_claim_device1():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/claim/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            'device_key': device1_device_key,
            'device_name': "testDeviceName"
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_claim_device1"])
def test_is_claimed_device1_after_being_claimed():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/is_claimed/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                "test_login_user2",
                "test_create_device2_by_admin"
                ])
def test_claim_device2():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/claim/'
    url = base_url + api_url
    payload = {
            'serial_number': device2_serial_number,
            'device_key': device2_device_key,
            'device_name': "testDeviceName2"
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                "test_login_user2",
                "test_create_device1_by_admin",
                "test_claim_device1",
                ])
def test_claim_a_claimed_device():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/claim/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            'device_key': device1_device_key,
            'device_name': "testDeviceName2"
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 200) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                        "test_login_user1",
                        "test_create_device1_by_admin",
                        "test_claim_device1",
                        ])
def test_fetch_claimed_devices():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and jresponse[0]["id"] == device1_id ) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_claim_device1",
                        ])
def test_fetch_claimed_device1_by_user1():
    # Fetch CLAIMED device1 by user1 (api/device/device1_id)
    # since user must be able to fetch this particular device info
    api_url = 'api/device/' + str(device1_id)
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and jresponse["id"] == device1_id ) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_login_user2",
                        "test_claim_device1",
                        ])
def test_fetch_claimed_device1_by_user2():
    # Fetch CLAIMED device1 by user2 (api/device/device1_id)
    # since user must NOT be able to fetch this particular device info
    # request must return Forbidden error
    api_url = 'api/device/' + str(device1_id)
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user2_auth_token
            }
    response = requests.get(url, headers=headers)
    #print(response.status_code)
    #print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

# =================== test device PUT, PATCH calls ===========

@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                "test_create_device1_by_admin"
                                ])
def test_update_device1():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            'name': 'testDevice_updated',
            'serial_number': device1_serial_number,
            'device_key': device1_device_key,
            'pin': '123',
            'port': '25',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.4',
            'public_key': 'AAAAB3NzaC1yc2EAAA',
            'local_ip_address':'1.2.3.4',
            'local_token':'sbsee'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.put(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                "test_create_device1_by_admin"
                                ])
def test_partial_update_device1():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            'port': '255',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.5',
            'public_key': 'AAAAB3NzaC1yc2EAAA'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

# ======== test unauthorized device GET, PUT, PATCH calls ===========
@pytest.mark.dependency(depends=[
                        "test_login_user2",
                        "test_claim_device1",
                        ])
def test_fetch_claimed_device_by_user2():
    # get a list of all CLAIMED devices (api/device/)
    # at this point there are no devices returned
    # since user has not yet claimed any device
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user2_auth_token
            }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                ])
def test_update_device1_by_user2():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            'name': 'testDevice_updated',
            'serial_number': '111111',
            'device_key': device1_device_key,
            'pin': '123',
            'port': '25',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.4',
            'public_key': 'AAAAB3NzaC1yc2EAAA',
            'local_ip_address':'1.2.3.4',
            'local_token':'sbsee'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.put(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                ])
def test_partial_update_device1_by_user2():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            'port': '255',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.5',
            'public_key': 'AAAAB3NzaC1yc2EAAA'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

# ====== test friend POST, GET, PUT, PATCH ===========

@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                ])
def test_add_friend1_by_user1():
    # delete user1
    # api_url = 'api/frined/' + str(device1_id) + '/'
    global friend1_id
    api_url = 'api/friend/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            "email": "TestFriend@we-pn.com",
            # "telegram_handle": "@inja",
            #"has_connected": "False",
            "passcode": "test",
            #"cert_id": "wp.23",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    friend1_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass


# test must fail with empty cert_id ceritificate
@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                ])
def test_add_friend1_by_user1_empty_cert_id():
    # delete user1
    # api_url = 'api/frined/' + str(device1_id) + '/'
    global friend1_id
    api_url = 'api/friend/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            "email": "TestFriend2@we-pn.com",
            # "telegram_handle": "@inja",
            #"has_connected": "False",
            "passcode": "test2",
            "cert_id": "",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_add_friend1_by_user1",
                                ])
def test_get_friends_by_user1():
    # delete user1
    api_url = 'api/friend/'
    url = base_url + api_url

    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    friend1_id == jresponse[0]['id']
    assert((response.status_code == 200) and (friend1_id == jresponse[0]['id'])) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_add_friend1_by_user1",
                                ])
def test_partial_update_friend1_by_user1():
    global friend1_id
    api_url = 'api/friend/' + str(friend1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            "email": "TestFriendUpdated@we-pn.com",
            # "telegram_handle": "@inja",
            #"has_connected": "False",
            "passcode": "testUpdated",
            #"cert_id": "wp.23",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    #friend1_id = jresponse['id']
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_add_friend1_by_user1",
                                ])
def test_fetch_friend1_by_user1():
    # try to fetch friend1 info using the unauthorized user2
    # api_url = 'api/frined/' + str(device1_id) + '/'
    api_url = 'api/friend/' + str(friend1_id) + '/'
    url = base_url + api_url

    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

# ===== test friend POST, GET, PUT, PATCH by Unauthorized User =========
@pytest.mark.dependency(depends=[
                                "test_add_friend1_by_user1",
                                ])
def test_fetch_friend1_by_user2():
    # try to fetch friend1 info using the unauthorized user2
    # api_url = 'api/frined/' + str(device1_id) + '/'
    api_url = 'api/friend/' + str(friend1_id) + '/'
    url = base_url + api_url

    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                                "test_add_friend1_by_user1",
                                ])
def test_partial_update_friend1_by_user2():
    global friend1_id
    api_url = 'api/friend/' + str(friend1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            "email": "TestFriendUpdated@we-pn.com",
            # "telegram_handle": "@inja",
            #"has_connected": "False",
            "passcode": "malicous",
            #"cert_id": "wp.23",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    #friend1_id = jresponse['id']
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

#==================== test Heartbeat =================

# hearbeat with correct device_key and serial
@pytest.mark.dependency(depends=[
                            "test_claim_device1",
                            "test_update_device1"])
def test_heartbeat_device1():
    api_url = 'api/device/heartbeat/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            'device_key': device1_device_key,
            'diag_code': '127',
            'local_ip_address': '196.168.1.1',
            'pin': '1234',
            'port': '4001',
            'status': '1',
            'ip_address': '155.125.87.23',
            'software_version': '1.4.3',
            'local_token': 'abcde',
            'last_seen': str(datetime.now(timezone.utc))
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

# hearbeat with wrong device_key
@pytest.mark.dependency(depends=[
                            "test_claim_device1",
                            "test_update_device1"])
def test_heartbeat_device1_wrong_key():
    api_url = 'api/device/heartbeat/'
    url = base_url + api_url
    payload = {
            'serial_number': device1_serial_number,
            'device_key': 'WRONGKEY',
            'diag_code': '127',
            'local_ip_address': '196.168.1.1',
            'pin': '1234',
            'port': '4001',
            'status': '1',
            'ip_address': '155.125.87.23',
            'software_version': '1.4.3',
            'local_token': 'abcde',
            'last_seen': str(datetime.now(timezone.utc))
            }

    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 401) #nosec: assert is a legit check for pytest
    pass

#=================Test experiments ===============

@pytest.mark.dependency(depends=[
                        "test_claim_device1",
                        ])
def test_create_experiment_for_device1():
    global experiment1_id
    api_url = 'api/experiment/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    password = 'test'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    input = {'experiment_name':'port_test',
                'port':port,
                'password':password
                }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'input': input,
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    experiment1_id = jresponse['id']
    time.sleep(20)
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_claim_device2",
                        ])
def test_create_experiment_for_device2():
    global experiment2_id
    api_url = 'api/experiment/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    password = 'test'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    input = {'experiment_name':'port_test',
                'port':port,
                'password':password
                }
    payload = { 'serial_number': device2_serial_number,
                'device_key': device2_device_key,
                'input': input,
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    experiment2_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                        "test_create_experiment_for_device1",
                        ])
def test_list_experiments_for_device1():
    #global experiment1_id
    api_url = 'api/experiment/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    sameID = (str(experiment1_id) == str(jresponse[0]['id']))
    assert(response.status_code == 200 and sameID) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_create_experiment_for_device1",
                        ])
def test_retreive_experiment_for_device1():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    sameID = (str(experiment1_id) == str(jresponse['id']))
    assert(response.status_code == 200 and sameID) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                            "test_create_experiment_for_device1",
                            ])
def test_get_experiment_result_for_device1():
    global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/result/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }

    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    result = jresponse['result']
    assert(result['experiment_result'] == 'False') #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_claim_device1",
                        ])
def test_create_experiment_wrong_device_credentials():
    global experiment1_id
    api_url = 'api/experiment/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    password = 'test'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    input = {'experiment_name':'port_test',
                'port':port,
                'password':password
                }
    payload = { 'serial_number': device1_serial_number,
                'device_key': 'wrongkey',
                'input': input,
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #experiment1_id = jresponse['id']
    assert(response.status_code == 401) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_create_experiment_for_device1",
                        ])
def test_partial_update_experiment_for_device1():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    result = {'partial_update':'done'
                }
    payload = { 'id': experiment1_id,
                'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'result': result
               }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    sameID = (str(experiment1_id) == str(jresponse['id']))
    assert(response.status_code == 200 and sameID) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_create_experiment_for_device1",
                        ])
def test_partial_update_experiment_with_wrong_key():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    result = {'partial_update':'done'
                }
    payload = { 'serial_number': device1_serial_number,
                'device_key': 'wrongkey',
                'result': result
               }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    # sameID = (str(experiment1_id) == str(jresponse['id']))
    assert(response.status_code == 401) #nosec: assert is a legit check for pytest
    pass


#=================Test Message API ===============

@pytest.mark.dependency(depends=[
                        "test_claim_device1",
                        ])
def test_create_message_for_device1():
    global message1_id
    api_url = 'api/message/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    password = 'test'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    message_body = {'experiment_name':'shadowsocks_test',
                    'port':port,
                    'password':password
                    }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'message_body': message_body,
                'destination':'DEVICE'
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    message1_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                        "test_claim_device1",
                        ])
def test_create_message_wrong_device_credentials():
    #
    api_url = 'api/message/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    password = 'test'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    message_body = {'experiment_name':'shadowsocks_test',
                    'port':port,
                    'password':password
                    }
    payload = { 'serial_number': device1_serial_number,
                'device_key': 'wrongdevicekey',
                'message_body': message_body,
                'destination':'DEVICE'
               }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 401) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_retrieve_message_for_device1():
    api_url = 'api/message/' + str(message1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and (message1_id == jresponse['id'])) #nosec: assert is a legit check for pytest
    pass

#fetch all read messages
# there must be zero messages that are read yet
@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_list_read_messages_for_device1():
    api_url = 'api/message/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'is_read':True
               }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and (len(jresponse)==0)) #nosec: assert is a legit check for pytest
    pass

# update
@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_update_message_for_device1():
    api_url = 'api/message/' + str(message1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    payload = {'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               'message_body':{'hello': 'world'},
               'message_response':{'good': 'bye'},
               'destination':'APP',
               'is_read':True
               }
    response = requests.put(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_partial_update_message_for_device1():
    api_url = 'api/message/' + str(message1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    payload = {'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'is_expired':True
               }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

# get only read messages after
@pytest.mark.dependency(depends=[
                        "test_update_message_for_device1",
                        ])
def test_list_read_messages_for_device1_after_being_read():
    api_url = 'api/message/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'is_read':True
               }
    response = requests.get(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 200 and (jresponse[0]['id']==message1_id)) #nosec: assert is a legit check for pytest
    pass

# ====================Security tests =================
# IDOR test 1
# Attempt to change device key
# the following test must throw a permission denied error
@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                "test_create_device1_by_admin"
                                ])
def test_partial_update_device1_with_another_device_id():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url

    # update device with following default parameters
    payload = {
            'id': device2_id,
            'port': '255',
            'status': '1',
            'ip_address': '1.1.1.1',
            'software_version': '0.1.5',
            'public_key': 'AAAAB3NzaC1yc2EAAA'
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_login_user2"])
def test_partial_update_user2_with_user1_id():
    # create user object
    # usr1 = User(baseurl, user_name, password, client_id, client_secret)
    # global user2_id
    api_url = 'api/user/' + str(user2_id) + '/'
    url = base_url + api_url
    payload = {
            'id': user1_id,
            'email': 'attacker_email@test.com',
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    #print(payload)
    response = requests.patch(url, json=payload, headers=headers)
    jresponse = response.json()
    print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    #user2_auth_token = jresponse['access_token']
    # user2_id = jresponse['id']
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass

#use a experiment2_id which delongs to device id 2
# when autheticating with device 1 credentials
@pytest.mark.dependency(depends=[
                        "test_create_experiment_for_device1",
                        "test_create_experiment_for_device2",
                        ])
def test_partial_update_experiment_for_device1_with_another_exp_id():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            }
    result = {'partial_update':'done'
                }
    payload = { 'id': experiment2_id,
                'serial_number': device1_serial_number,
                'device_key': device1_device_key,
                'result': result
               }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    # sameID = (str(experiment1_id) == str(jresponse['id']))
    assert(response.status_code == 401) #nosec: assert is a legit check for pytest
    pass

# IDOR test for friend
def test_partial_update_friend1_with_friend2_id():
    #global experiment1_id
    print("Friend 1 ID is" + str(friend1_id))
    print("Friend 2 ID is" + str(friend2_id))
    api_url = 'api/friend/' + str(friend1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    payload = { 'id': friend2_id,
                'passcode': 'hacked'
               }
    response = requests.patch(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    #print('experiment id is :' + str(experiment1_id))
    id_not_changed = (str(friend1_id) == str(jresponse['id']))
    assert(response.status_code == 200 and id_not_changed) #nosec: assert is a legit check for pytest
    pass

# attempt to create a user account with short password
@pytest.mark.dependency()
def test_create_user_with_bad_password():
    print("creating user with username: sillyman@we-pn.com")
    # create user object
    # usr1 = User(baseurl, user_name, password, client_id, client_secret)
    api_url = 'api/user/'
    url = base_url + api_url
    payload = {
            'email': 'sillyman@we-pn.com',
            'firstname': 'Silly',
            'lastname': 'Mann',
            'password': '1234',
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    #print(jresponse['access_token'])
    #user1_auth_token = jresponse['access_token']
    # user1_id = jresponse['id']
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

# ===== login attempt rate limit test ============
@pytest.mark.dependency()
def test_create_victim_user():
    global victim_user_id
    api_url = 'api/user/'
    url = base_url + api_url
    payload = {
            "email":"bruteforce_victim@we-pn.com",
            "password":"trytoguessmypassword9673",
            }
    # print(payload)
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    #print(payload)
    response = requests.post(url, json=payload, headers=headers)
    jresponse = response.json()
    #print(response.status_code)
    print(jresponse)
    print(response.status_code)
    print(response.text)
    #print(jresponse['access_token'])
    #user2_auth_token = jresponse['access_token']
    victim_user_id = jresponse['id']
    assert(response.status_code == 201) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_create_victim_user"])
def test_login_rate_limit():
    api_url = 'o/token/'
    url = base_url + api_url
    payload = {
            "grant_type":"password",
            "username":"bruteforce_victim@we-pn.com",
            "password":"clearlywrong",
            "client_id":client_id,
            "client_secret":client_secret}

    headers = {
            "content-type": "application/json"
            }
    for i in range(15):
        response = requests.post(url, json=payload, headers=headers)
    print(response.status_code)
    print(response.text)
    payload = {
            "grant_type":"password",
            "username":"bruteforce_victim@we-pn.com",
            "password":"trytoguessmypassword9673",
            "client_id":client_id,
            "client_secret":client_secret}
    response = requests.post(url, json=payload, headers=headers)
    print("==========")
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_create_victim_user"])
def test_delete_victim_user():
    # delete user1
    api_url = 'api/user/' + str(victim_user_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + admin_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass

# try to email bomb user 1 through forgot_password API
@pytest.mark.dependency(depends=["test_create_victim_user"])
def test_email_bomb_with_forgot_password():
    api_url = 'api/user/forgot_password/'
    url = base_url + api_url
    payload = {
            "email":"bruteforce_victim@we-pn.com",
            }
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8'
        }
    for i in range(1):
        response = requests.post(url, json=payload, headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 400) #nosec: assert is a legit check for pytest
    pass

#=====================unclaim device tests============
# let's first attempt to unclaim/remove device
# by unauthroized user 2
@pytest.mark.dependency(depends=[
                                "test_claim_device1",
                                "test_login_user2"
                                ])
def test_unclaim_device1_by_user2():
    # delete device by user1
    api_url = 'api/device/' + str(device1_id) + '/unclaim/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 200) #nosec: assert is a legit check for pytest
    pass


# let's first attempt to unclaim device
# by authroized user 1
@pytest.mark.dependency(depends=["test_claim_device1"])
def test_unclaim_device1_by_user1():
    # delete device by user1
    api_url = 'api/device/' + str(device1_id) + '/unclaim/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.get(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 200) #nosec: assert is a legit check for pytest
    pass

# ============ detele messages and experiments ===== #

@pytest.mark.dependency(depends=[
                            "test_create_experiment_for_device1",
                            ])
def test_delete_experiment1_with_user1():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }

    payload = { 'serial_number': device1_serial_number,
                'device_key': device1_device_key,
               }
    response = requests.delete(url, data=json.dumps(payload), headers=headers)
    print(response.status_code)
    print(response.text)
    jresponse = response.json()
    assert(response.status_code == 403) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                            "test_create_experiment_for_device1",
                            ])
def test_delete_experiment1_with_admin():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                            "test_create_experiment_for_device2",
                            ])
def test_delete_experiment2_with_admin():
    #global experiment1_id
    api_url = 'api/experiment/' + str(experiment2_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass


# delete messages
@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_delete_message_by_user1():
    api_url = 'api/message/' + str(message1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + user1_auth_token
            }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 403 ) #nosec: assert is a legit check for pytest
    pass


@pytest.mark.dependency(depends=[
                        "test_create_message_for_device1",
                        ])
def test_delete_message_by_admin():
    api_url = 'api/message/' + str(message1_id) + '/'
    url = base_url + api_url
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + admin_auth_token
            }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass


#=====================delete user tests============

@pytest.mark.dependency(depends=[
                                "test_login_user1",
                                "test_login_user2"
                                ])
def test_delete_user1_by_another_user():
    # delete user1
    api_url = 'api/user/' + user1_id + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 204) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=[
                            "test_login_user1",
                            "test_login_user2"
                            ])
def test_delete_user1_by_another_user():
    # delete user1 with user2
    # action is prohibited
    api_url = 'api/user/' + str(user1_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user2_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 204) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_login_user1"])
def test_delete_user1():
    # delete user1
    api_url = 'api/user/' + str(user1_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass

@pytest.mark.dependency(depends=["test_admin_login"])
def test_delete_user2_by_admin():
    # delete user1
    api_url = 'api/user/' + str(user2_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + admin_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass

# ========== delete device =============================

#let's attempt to delete device by user1
# it must fail - it is not permitted
@pytest.mark.dependency(depends=[
                                "test_login_user1",
                                "test_create_device1_by_admin"
                                ])
def test_delete_device1_by_user1():
    # delete device by user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + user1_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code != 204) #nosec: assert is a legit check for pytest
    pass

# lets delete the device1 using admin credential
@pytest.mark.dependency(depends=[
                                "test_admin_login",
                                "test_create_device1_by_admin"
                                ])
def test_delete_device1_by_admin():
    # delete user1
    api_url = 'api/device/' + str(device1_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + admin_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass

#lets delete the device2 using admin credential
@pytest.mark.dependency(depends=[
                                "test_admin_login",
                                "test_create_device2_by_admin"
                                ])
def test_delete_device2_by_admin():
    # delete user1
    api_url = 'api/device/' + str(device2_id) + '/'
    url = base_url + api_url
    headers = {
        'content-type': 'application/json',
        'Accept-Charset': 'UTF-8',
        'Authorization': 'Bearer ' + admin_auth_token
        }
    response = requests.delete(url, headers=headers)
    print(response.status_code)
    print(response.text)
    #jresponse = response.json()
    assert(response.status_code == 204) #nosec: assert is a legit check for pytest
    pass

# ####################################################################


import requests
import json


class Experiment:
    # local setup
    # client_secret = 'local SECRET SECRET SECRET'
    # client_id = 'ID ID ID '
    #
    # server setup
    # client_secret = 'server SECRET SECRET SECRET'
    # client_id = 'ID ID ID '

    def __init__(self, base_url='', token=''):
        self.token = token
        if base_url:
            self.base_url = base_url
        else:
            # if no base URL is given use the following default URL
            self.base_url = 'https://api-dev.we-pn.com/'

    def experiment_get(self, token='', pk=''):

        if pk == '':
            api_url = 'api/experiment/'
        else:
            api_url = 'api/experiment/' + pk + '/'
        url = self.base_url + api_url

        if not token:
            token = self.token

        headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + token
            }
        r = requests.get(url, headers=headers)
        # print(r.text)
        if r.status_code == 200:
            # successful
            return r.json()
        else:
            # error occured
            return False


    def create(self, payload={
                    'serial_number': '1123',
                    'device_key': '12jhbhed3',
                    'input': '{}'
                    }):
        api_url = 'api/experiment/'
        url = self.base_url + api_url

        print(payload)
        headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8'
            }
        print(headers)
        r = requests.post(url, data=json.dumps(payload), headers=headers)
        print(r.text)
        # The request has been fulfilled and resulted in
        # a new resource being created.
        print('status code is ', r.status_code)
        if r.status_code == 201:
            # successful
            return r.json()
        else:
            # error occured
            return False


    def result(self, pk='1', payload = {
                    'serial_number': '1123',
                    'device_key': '12jhbhed3'
                    }):

        api_url = 'api/experiment/' + pk + '/result/'
        url = self.base_url + api_url

        print(payload)
        headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'Authorization': 'Bearer ' + self.token
            }

        r = requests.post(url, data=json.dumps(payload), headers=headers)
        print(r.text)
        # The request has been fulfilled and resulted in
        # a new resource being created.
        if r.status_code == 200:
            # successful
            return r.json()
        else:
            # error occured
            return False


# r = requests.get(url)
# r = requests.post(url, data=json.dumps(payload), headers=headers)

if __name__ == "__main__":
    serial_number = 'Q6X***Z1R'
    device_key = '3AS***3DSUL'
    password = '965***2407'
    port = '7002'
    # input = {'experiment_name':'port_test', 'port':'1230'}
    input = {'experiment_name':'shadowsocks_test', 'port':port, 'password':password}
    payload = {'serial_number': serial_number,
               'device_key': device_key,
               'input': input}
    e = Experiment()
    resp = e.create(payload)
    print('experiment initiated')
    print(resp)

    # fetch result
    pk = str(resp['id'])
    payload = {'serial_number': serial_number,
               'device_key': device_key}
    resp = e.result(pk, payload)
    print('experiment fetched')
    print(resp)
    # sign up was successful

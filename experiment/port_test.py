import socket

def test_port_open(ip_address, port):
    try:
        print("running port test....")
        print('IP address is ', ip_address)
        print('Port is ', port)
        s = socket.create_connection((ip_address, port), 10)
        s.settimeout(30)
        s.sendall(b'test\n')
    except OSError:
        return {'experiment_result': 'False'}
    else:
        return {'experiment_result': 'True'}

from rest_framework import serializers
from datetime import datetime
from .models import Experiment


class ExperimentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True, read_only=True)
    input = serializers.JSONField(required=True)
    result = serializers.JSONField(required=False)
    initiated_time = serializers.DateTimeField(required=False, allow_null=True)
    finished_time = serializers.DateTimeField(required=False, allow_null=True)

    class Meta:
        model = Experiment
        fields = (
            'id',
            'input',
            'result',
            'initiated_time',
            'finished_time',
        )

    def create(self, validated_data):
        print(validated_data)
        return Experiment.objects.create(
                        device = validated_data['device'],
                        input = validated_data['input'],
        )

from django.contrib import admin
from .models import Experiment

# Register your models here.

class ExperimentAdmin(admin.ModelAdmin):
    list_display = ('id', 'device', 'initiated_time', 'finished_time', 'completed', 'input', 'result')
    search_fields = ['device__user__email']

# Register your models here.
admin.site.register(Experiment, ExperimentAdmin)

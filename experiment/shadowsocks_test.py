import portpicker
from subprocess import Popen, PIPE
from time import sleep

# pip install portpicker (portpicker 1.3.1)
# sudo snap install shadowsocks-libev

def url_test(local_port):
    url = 'https://www.google.com'
    #curl -s -o /dev/null -w "%{http_code}" -x socks5h://localhost:4008 https://www.google.com
    socks = 'socks5h://localhost:%s' % local_port
    command = ['curl', '-s', '-o', '/dev/null', '-w', '"%{http_code}"', '-x', socks, url]
    print(command)
    p2 = Popen(command, stdout=PIPE)
    result = p2.communicate()[0]
    if result.decode("utf-8") == '"200"':
        print('success!')
        return True
    else:
        print('failure')
        return False

def test_shadowsocks(ip, port, password):
    retries = 3
    local_port = portpicker.pick_unused_port()
    #ss-local -s 157.131.253.39 -p 4009 -l 4008 -k 6956107546 -m aes-256-gcm -v
    shadowsocks_client_command = ['/usr/bin/snap', 'run', 'shadowsocks-libev.ss-local', '-s', ip, '-p', port, '-l', str(local_port), '-k', password, '-m', 'aes-256-gcm', '-v']
    print(shadowsocks_client_command)
    p1 = Popen(shadowsocks_client_command, stdout=PIPE)
    # print(p1.returncode)
    #print(p1.communicate()[0])
    print('sleeping for 3 seconds....')
    sleep(3)
    while retries > 0:
        if url_test(local_port):
            print('shadowsocks test was successful')
            p1.kill()
            return {'experiment_result': 'True'}
        else:
            retries -= 1
            print('retry ', retries)
    print('shadowsocks test failed')
    p1.kill()
    return {'experiment_result': 'False'}


if __name__ == '__main__':
    ip = '157.131.253.39'
    port = '4009'
    password = '6956107546'
    test_shadowsocks(ip, port, password)

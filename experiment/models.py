from django.db import models

# Create your models here.
from device.models import Device
from django.utils import timezone
import datetime


class Experiment(models.Model):
    id = models.AutoField(primary_key=True)
    device = models.ForeignKey(Device, related_name='experiment_device', on_delete=models.SET_NULL, null=True, blank=True)
    initiated_time = models.DateTimeField('initiated on', null=True)
    finished_time = models.DateTimeField('finished on', null=True)
    completed = models.BooleanField(default=False)
    input = models.JSONField(null=True)
    result = models.JSONField(null=True)

    def __str__(self):
        return str(self.id)

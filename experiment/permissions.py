from rest_framework.permissions import BasePermission
from device.models import Device
from user.models import User


class IsValidDevice(BasePermission):
    """Only allow API calls with valid serial number and device key."""

    def has_permission(self, request, view):
        """Return True if it includes valid device credential."""
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        print(serial_number)
        print(device_key)
        queryset = Device.objects.filter(serial_number=serial_number,
                                        device_key=device_key)
        if queryset.exists():
            print("authentication passed")
            return True
        else:
            print("authentication failed")
            return False
        # print("request.user.email = " + request.user.email)
        # print("obj.email = " + obj.email)
        #return obj.user == request.user
    def has_object_permission(self, request, view, obj):
        """Return True if device credentials match with obj"""
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        print(serial_number)
        print(device_key)
        #only allow operations on objects with same IDs as in request
        # this will prevent IDOR attacks
        if request.data.get("id"):
            sameID = (str(obj.id) == str(request.data.get("id")))
        else:
            sameID = True
        queryset = Device.objects.filter(serial_number=serial_number,
                                        device_key=device_key)
        if queryset.exists():
            for device in queryset:
                if (serial_number == obj.device.serial_number) and \
                        (device_key == obj.device.device_key) and \
                        sameID:
                        return True
                else:
                    print("authentication failed at object level")
                    return False
        else:
            print("authentication failed - no device with such credential")
            return False



# class IsSame(BasePermission):
#     """Custom permission class to allow only owners to edit them."""
#
#     def has_object_permission(self, request, view, obj):
#         """Return True if permission is granted to the device owner."""
#         print("request.serial_number = " + request.serial_number)
#         print("request.device_key = " + request.device_key)
#         print("obj.device.serial_number = " + obj.device.serial_number)
#         if isinstance(obj, Device):
#             return (obj.serial_number == request.serial_number) and \
#                    (obj.device_key == request.device_key)
#         elif isinstance(obj, User):
#             return (obj.device.serial_number == request.serial_number) and \
#                    (obj.device.device_key == request.device_key)
#         else:
#             return False
#         # print("request.user.email = " + request.user.email)
#         # print("obj.email = " + obj.email)
#         #return obj.user == request.user

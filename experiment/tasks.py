# Create your tasks here

from celery import shared_task
from .models import Experiment
from device.models import Device
from django.core.mail import send_mail
from datetime import datetime, timezone, timedelta
import socket
from .shadowsocks_test import test_shadowsocks
from .port_test import test_port_open

@shared_task
def start_experiment(experiment_id):
    queryset = Experiment.objects.filter(id=experiment_id)
    if queryset.exists():
        for experiment in queryset:
            if experiment.input['experiment_name'] == 'port_test':
                print("running port test ....")
                experiment.initiated_time=datetime.now(timezone.utc)
                port = experiment.input['port']
                ip_address = experiment.device.ip_address
                experiment.result = test_port_open(ip_address, port)
            if experiment.input['experiment_name'] == 'shadowsocks_test':
                print("running shadowsocks test ....")
                experiment.initiated_time=datetime.now(timezone.utc)
                port = experiment.input['port']
                password = experiment.input['password']
                ip_address = experiment.device.ip_address
                experiment.result = test_shadowsocks(ip_address, port, password)
            experiment.finished_time=datetime.now(timezone.utc)
            experiment.completed = True
            experiment.save()
            # do some work

# Create your tasks here

from celery import shared_task
from .models import Device
from friend.models import Friend
from django.core.mail import send_mail
from datetime import datetime, timezone, timedelta
from actions import actions
import ipaddress
import random
import string
import time

@shared_task
def check_device_pulse():
    print("task is running...")
    queryset = Device.objects.all()
    if queryset.exists():
        for device in queryset:
            print("Time lapsed since last heartbeat for device ID ", device.id, ':', datetime.now(timezone.utc)-device.last_seen)
            if ((datetime.now(timezone.utc)-device.last_seen)> timedelta(minutes=55*25)) \
                and (device.owner_notified_offline==False) and (device.permission_to_notify_owner == True):
                if device.user is not None:
                    recipient_email = device.user.email
                    print("Sending offline notification email to ", recipient_email)
                    email_subject = "WEPN Device " + device.name + " is Offline"
                    email_body = "It seems like your device (" + device.name + \
                                 ") is offline and not connected. It was last seen on " + \
                                 device.last_seen.strftime("%d/%m/%Y %H:%M:%S") + "UTC."
                    send_mail(email_subject, email_body, '"WEPN Device" <devices@we-pn.com>',[recipient_email], fail_silently=False)
                    device.owner_notified_offline = True
                    device.save()

@shared_task
def sync_credentials(device_id, credentials):
    device_queryset = Device.objects.filter(id=device_id)
    if device_queryset.exists():
        for device in device_queryset:
            queryset = Friend.objects.filter(user=device.user)
    for k, v in credentials.items():
        cert_found = False
        print("the cert ID is", k)
        print("the hash value is", v)
        for friend in queryset:
            if friend.cert_id == k:
                cert_found = True
                # check hash values
                # if hash is different update hash on server
                if friend.cert_hash != v:
                    friend.cert_hash = v
                    friend.save()
                break
        if cert_found is True:
            continue
        else:
            # cert_id was present on device but not on server
            print("delete cert id " + k + "from the device")
            actioninstance = actions.Actions()
            msg = { 'action': "delete_user",
                    'cert_name': k,
                    'email': "noreply@we-pn.com"
                    }
            actioninstance.publish_mqtt_msg(device.id, msg)
            actioninstance.disconnect()
    for friend in queryset:
        cred_found = False
        for k, v in credentials.items():
            if friend.cert_id == k:
                cred_found = True
                break
        if cred_found is True:
            # check the next friend
            continue
        else:
            print("add cert id " + friend.cert_id + "to the device")
            actioninstance = actions.Actions()
            msg = { 'action': "add_user",
                    'cert_name': friend.cert_id,
                    'email': friend.email,
                    'passcode': friend.passcode,
                    'language': friend.language,
                    'uuid': str(friend.unique_id),
                    'subscribed': friend.subscribed,
                    'id':friend.id
                    }
            actioninstance.publish_mqtt_msg(device.id, msg)
            actioninstance.disconnect()
    # check if friend with cert id k exists
    # if so check is hash value is the same as v
    # if cert id does not exist in db, delete on device
    # if there's any cert id in db that does not match cert
    # IDs in heartbeat, add them them to the device

#syncronous task
def wipe_friends(device_id):
    device_queryset = Device.objects.filter(id=device_id)
    if device_queryset.exists():
        for device in device_queryset:
            queryset = Friend.objects.filter(user=device.user)
            actioninstance = actions.Actions()
            for friend in queryset:
                print("delete friend " + str(friend.email) + "from the device")
                msg = { 'action': "delete_user",
                        'cert_name': friend.cert_id,
                        'email': friend.email,
                        'id':friend.id
                        }
                actioninstance.publish_mqtt_msg(device.id, msg)
            actioninstance.disconnect()


#syncronous task
def wipe_device(device_id):
    device_queryset = Device.objects.filter(id=device_id)
    if device_queryset.exists():
        for device in device_queryset:
            print("send mqtt commpand to wipe off info from device")
            actioninstance = actions.Actions()
            msg = {'action': 'wipe_device'}
            actioninstance.publish_mqtt_msg(device.id, msg)
            device.user = None
            device.save()
            N = 20
            random_key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N))
            time.sleep(2)
            if not actioninstance.set_mqqt_creds(device.id, random_key):
               print('changing MQTT password failed')
            actioninstance.disconnect()


@shared_task
def update_usage_status(device_id, usage_status):
    device_queryset = Device.objects.filter(id=device_id)
    if device_queryset.exists():
        for device in device_queryset:
            queryset = Friend.objects.filter(user=device.user)
    for k, v in usage_status.items():
        print("the cert ID is", k)
        print("status is", v)
        for friend in queryset:
            if friend.cert_id == k:
                friend.usage_status = v
                if v > 0:
                    friend.has_connected = True
                friend.save()

@shared_task
def notify_user_online(device_id):
    #TODO change this code to analyze IP change history
    queryset = Device.objects.filter(id=device_id)
    if queryset.exists():
        for device in queryset:
            time_diff = datetime.now(timezone.utc)-device.last_seen
            if time_diff > timedelta(minutes=55):
                #if device is claimed and has a user
                if (device.user is not None) and (device.permission_to_notify_owner == True):
                    recipient_email = device.user.email
                    print("Preparing to send email to ", recipient_email)
                    email_subject = "WEPN Device " + device.name + " is back online"
                    email_body = "It seems like your device (" + device.name + \
                                 ") is online again. It was last seen on " + \
                                  device.last_seen.strftime("%d/%m/%Y %H:%M:%S") + \
                                  " UTC. Our systems shows this device has been offline since about " + \
                                  humanize.naturaltime(time_diff) + \
                                  ". You can opt out of email notifications through the app if you wish."
                    send_mail(email_subject, email_body, '"WEPN Device" <devices@we-pn.com>',[recipient_email], fail_silently=False)
                    device.owner_notified_offline = False

@shared_task
def notify_ip_change(device_id, ip_address):
    device_queryset = Device.objects.filter(id=device_id)
    if device_queryset.exists():
        for device in device_queryset:
            # add IP adress history table to mnitor IP changes over time
            try:
                ipaddress.ip_interface(ip_address)
                if device.ip_address != ip_address:
                    # resend emails to all Friends
                    print("New IP address detected!")
                    actioninstance = actions.Actions()
                    queryset = Friend.objects.filter(user=device.user)
                    print("sending MQTT command to device" + str(device.id))
                    # notify users of IP change and send them new access key
                    for friend in queryset:
                        msg = { 'action': "add_user",
                        		'cert_name': friend.cert_id,
                        		'email': friend.email,
                        		'passcode': friend.passcode,
                        		'language': friend.language,
                                'uuid': str(friend.unique_id),
                                'subscribed': friend.subscribed,
                                'id':friend.id,
                                'config':friend.config
                        		}
                        actioninstance.publish_mqtt_msg(device.id, msg)
                    actioninstance.disconnect()
                    # update IP address
                    device.ip_address = ip_address
                    device.save()
                else:
                    print("Same ip address!")
            except ValueError as value_error:
                    print(value_error)

# @shared_task
# def check_device_pulse(device_id):
#     queryset = Device.objects.filter(id=device_id)
#     if queryset.exists():
#         for device in queryset:
#             print("Time lapsed since last heartbeat is ", datetime.now(timezone.utc)-device.last_seen)
#             if (datetime.now(timezone.utc)-device.last_seen)> timedelta(minutes=19):
#                 if device.user.email is not None:
#                     recipient_email = device.user.email
#                     print("Sending offline notification email to ", recipient_email)
#                     email_subject = "WEPN Device is Offline"
#                     email_body = "It seems like your device is offline and not connected. It was last seen on " + device.last_seen.strftime("%d/%m/%Y %H:%M:%S") + "UTC"
#                     send_mail(email_subject, email_body, 'devices@we-pn.com',[recipient_email], fail_silently=False)
#
#             device.celery_task_running = False
#             device.save()

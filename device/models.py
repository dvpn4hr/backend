from django.db import models
# Create your models here.
from django.utils import timezone
import datetime

#from django.apps import apps
#apps.get_model('user.User')
#User = apps.get_model('user','User')

class Device(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey('user.User', related_name='user', on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=200, null=False, blank=False)
    last_seen = models.DateTimeField('last seen online', null=True)
    ip_address = models.GenericIPAddressField(null=True, default='0.0.0.0')
    local_ip_address = models.GenericIPAddressField(null=True, default='0.0.0.0')
    port = models.CharField(max_length=20, null=True, blank=True)
    serial_number = models.CharField(max_length=20, null=False, blank=False, unique=True)
    device_key = models.CharField(max_length=1024, null=False, blank=False, default=0)
    software_version = models.CharField(max_length=8, null=True, blank=True, default='')
    public_key = models.CharField(max_length=1024, null=True, blank=False)
    pin = models.CharField(max_length=20, null=True, blank=True)
    local_token = models.CharField(max_length=20, null=True, blank=True, default='')
    status = models.IntegerField(null=True, blank=True, default=1)
    diag_code = models.IntegerField(null=True, blank=True, default=0)
    local_network_connected = models.BooleanField(default=False)
    owner_notified_offline = models.BooleanField(default=True)
    permission_to_notify_owner = models.BooleanField(default=True)
    internet_connected = models.BooleanField(default=False)
    services_running = models.BooleanField(default=False)
    port_opened = models.BooleanField(default=False)
    MQTT_connected = models.BooleanField(default=False)
    VPN_selftest = models.BooleanField(default=False)
    is_claimed = models.BooleanField(default=False)
    note = models.TextField(null=True, blank=True, default=0)
    # status 0 Service: Stopped RPI: Off
    # status 1 Service: Stopped RPI: Up
    # status 2 Service: Running RPI: Up
    # status 3 Service: Stopped RPI: Restarting

    def __str__(self):
        return self.name

    def set_status(self):
        if datetime.timedelta(minutes=32) < (timezone.now() - self.last_seen):
            self.status = 0

    # objects = DeviceManager()
    # primary_key =True
    #
    # def clean(self):
    #      if (self.name == 'test'):
    #              raise ValidationError(_('No tests allowede!'))
# class IPAddress(models.Model):
#     id = models.AutoField(primary_key=True)
#     device = models.ForeignKey(Device, related_name='device', on_delete=models.SET_NULL, null=True)
#     last_seen = models.DateTimeField('last seen online', null=True)
#     ip_address = models.GenericIPAddressField()

from django.apps import AppConfig

class DeviceConfig(AppConfig):
    name = 'device'

    def ready(self):
        import device.signals

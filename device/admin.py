from django.contrib import admin

# Register your models here.
from .models import Device
from actions import actions
import string
import random


def unclaim_device(modeladmin, request, queryset):
    for device in queryset:
        device.user = None
        device.save()
        # send mqtt commpand to wipe off info from device
        actioninstance = actions.Actions()
        msg = {'action': 'wipe_device'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        N = 20
        random_key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N))
        if not actioninstance.set_mqqt_creds(device.id, random_key):
            print('changing MQTT password failed')
unclaim_device.short_description = "Unclaim selected devices"


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user', 'serial_number', 'ip_address', 'status')
    list_filter = ['last_seen']
    search_fields = ['name', 'serial_number', 'user__email']
    actions = [unclaim_device]
# Register your models here.
admin.site.register(Device, DeviceAdmin)

from rest_framework import serializers
from datetime import datetime
from .models import Device


class DeviceSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True, read_only=True)
    name = serializers.CharField(max_length=20)
    ip_address = serializers.IPAddressField(required=False,default='0.0.0.0')
    local_ip_address = serializers.IPAddressField(required=False,default='0.0.0.0')
    port = serializers.CharField(max_length=20,required=False,default='')
    pin = serializers.CharField(max_length=20, required=False, write_only=True, default=1)
    local_token = serializers.CharField(max_length=20,required=False,default=None)
    software_version = serializers.CharField(max_length=20,required=False,default=None)
    status = serializers.IntegerField(required=False, allow_null=True,default=None)
    diag_code = serializers.IntegerField(required=False, allow_null=True,default=0)
    serial_number = serializers.CharField(max_length=20)
    last_seen = serializers.DateTimeField(required=False, allow_null=True, default=datetime.now())
    public_key = serializers.CharField(max_length=1024,required=False,default='')
    device_key = serializers.CharField(max_length=1024,write_only=True)
    permission_to_notify_owner = serializers.BooleanField(required=False)


    class Meta:
        model = Device
        fields = (
            'id',
            'name',
            'ip_address',
            'port',
            'local_token',
            'local_ip_address',
            'software_version',
            'status',
            'diag_code',
            'serial_number',
            'last_seen',
            'public_key',
            'pin',
            'device_key',
            'permission_to_notify_owner',
        )

    def create(self, validated_data):
        print(validated_data)
        return Device.objects.create(
                        name=validated_data['name'],
                        ip_address=validated_data['ip_address'],
                        port=validated_data['port'],
                        local_token=validated_data['local_token'],
                        local_ip_address=validated_data['local_ip_address'],
                        serial_number=validated_data['serial_number'],
                        software_version=validated_data['software_version'],
                        status=validated_data['status'],
                        last_seen=validated_data['last_seen'],
                        public_key=validated_data['public_key'],
                        device_key=validated_data['device_key'],
                        pin=validated_data['pin'],
        )

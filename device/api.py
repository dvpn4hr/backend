
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from django.shortcuts import get_object_or_404
from django.core.mail import send_mail

from .models import Device
from friend.models import Friend
from user.models import User
from .permissions import IsSame, IsValidDevice
from .serializers import DeviceSerializer
from .tasks import check_device_pulse, notify_ip_change
from .tasks import notify_user_online, sync_credentials, update_usage_status
from .tasks import wipe_device, wipe_friends
from actions import actions

import pprint
import logging
import random
import string
import ipaddress
import humanize
from datetime import datetime, timezone, timedelta
import time

# Get an instance of a logger
# logger = logging.getLogger(__name__)


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions this view requires
        """
        print('request action is ' + str(self.action))
        if self.action in ['list', 'retrieve', 'update', 'partial_update']:
            # Only allow same user to view/edit/update its own record
            # print('execution reaches here')
            permission_classes = [IsAuthenticated, IsSame]
        elif self.action == 'create':
            # allow anyone to create (sign up) a new user
            # print('setting user permission to admin')
            permission_classes = [IsAdminUser, ]
        elif self.action in [
                                'unclaim',
                                'reboot',
                                'start',
                                'restart',
                                'reload',
                                'stop'
                            ]:
            # print('setting up permissions for heartbeat')
            permission_classes = [IsAuthenticated, IsSame]
        elif self.action in ['diagnosis', 'is_claimed']:
            permission_classes = [AllowAny]
        elif self.action == 'heartbeat':
            permission_classes = [IsValidDevice]
        elif self.action == 'claim':
            print('only allow authenticated users to claim a device')
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    # def get_permissions(self):
    #     if self.request.method in ['GET']:
    #         return [IsAuthenticated()]
    #     if self.request.method in ['POST', 'DELETE']:
    #         return [IsAdminUser()]
    #     if self.request.method in ['PUT', 'PATCH']:
    #         return [AllowAny()]
    #     else:
    #         return [IsAdminUser()]

    def list(self, request):
        if request.user.is_authenticated:
            queryset = Device.objects.filter(user=request.user)
            # updating device status before responding to query
            for device in queryset:
                device.set_status()
                device.save()
            serializer = DeviceSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        if request.user.is_authenticated:
            queryset = Device.objects.all()
            device = get_object_or_404(queryset, pk=pk)
            if device.user == request.user:
                device.set_status()
                device.save()
                serializer = DeviceSerializer(device)
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    #@list_route(methods=['post'])
    @action(detail=False, methods=['post'], url_path='diagnosis')
    def diagnosis(self, request):
        code = request.data['device_code']
        print(code)
        if code > 127:
            content = {'error_description': 'invalid diagnostic code'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)
        else:
            state = []
            for i in range(7):
                state.append(bool(code & (1 << i)))
            details = [
                    {
                        'state': state[0],
                        'description': "Connected to the local network" if state[0] else "Not connected to the local network",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004907891-My-device-cannot-connect-to-my-local-network"
                    },
                    {
                        'state': state[1],
                        'description': "Connected to the Internet" if state[1] else "Not connecting to the Internet",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004851592-My-device-cannot-connect-to-the-Internet"
                    },
                    {
                        'state': state[2],
                        'description': "Services running" if state[2] else "Services not running",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004908011-Services-are-not-running-on-my-WEPN"
                    },
                    {
                        'state': state[3],
                        'description': "Services reachable from the Internet" if state[3] else "Services not reachable from the Internet",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004782992-My-WEPN-device-is-having-an-issue-with-port-forwarding-"
                    },
                    {
                        'state': state[4],
                        'description': "Connected to WEPN server" if state[4] else "Not connecting to WEPN server",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004908051-My-device-cannot-connect-to-WEPN-server"
                    },
                    {
                        'state': state[5],
                        'description': "VPN self-test passed" if state[5] else "VPN self-test failed",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004851452-VPN-self-test-failed-"
                    },
                    {
                        'state': state[6],
                        'description': "Device owner is set" if state[6] else "Device owner is not set",
                        'url': "https://wepn.zendesk.com/hc/en-us/articles/360004908311-Device-owner-is-not-set-"
                    },
                    ]
            return Response(details, status=status.HTTP_200_OK)

    #@list_route(methods=['post'])
    @action(detail=False, methods=['post'], url_path='is_claimed')
    def is_claimed(self, request):
        # print(request.data)
        queryset = Device.objects.filter(serial_number=request.data['serial_number'].upper())
        # print(queryset)
        if queryset.exists():
            for device in queryset:
                # print(device.pin)
                # if request.data['device_key'] != device.device_key:
                #     print('invalid device key')
                #     content = {'invalid device key'}
                #     return Response(content, status=status.HTTP_403_FORBIDDEN)
                #
                # If device already has an owner AND the owner is not the same
                # as the user claiming the device, then reject the request
                # Proceed with the claimp process if the device has no owner
                # OR the current owner is the same as who is making the
                # request
                if (device.user is None):
                    content = {'description': 'Device does not exist'}
                    return Response(content, status=status.HTTP_403_FORBIDDEN)
                else:
                    content = {'description': 'Device is claimed'}
                    return Response(content, status=status.HTTP_200_OK)
        else:
            content = {'description': 'Device does not exist'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

    #@list_route(methods=['post'])
    @action(detail=False, methods=['post'], url_path='claim')
    def claim(self, request):
        # print(request.data)
        queryset = Device.objects.filter(serial_number=request.data['serial_number'].upper())
        print(queryset)
        if queryset.exists():
            for device in queryset:
                # print(device.pin)
                # if request.data['device_key'] != device.device_key:
                #     print('invalid device key')
                #     content = {'invalid device key'}
                #     return Response(content, status=status.HTTP_403_FORBIDDEN)
                #
                # If device already has an owner AND the owner is not the same
                # as the user claiming the device, then reject the request
                # Proceed with the claimp process if the device has no owner
                # OR the current owner is the same as who is making the
                # request
                if (device.user is None) or (device.user == request.user):
                    print(request.user)
                    device_key = request.data['device_key'].upper()
                    # calculate checksum and terminate process
                    # if exception happens
                    try:
                        cksum = self.checksum(device_key[:-1])
                        print("check sum is" + cksum)
                    except:
                        content = {'error_description': 'Invalid key. Double check the device key!'}
                        return Response(content, status=status.HTTP_403_FORBIDDEN)
                    # validate checksum against the last string
                    # character in device key.
                    if cksum is not device_key[-1]:
                        print('invalid checksum')
                        content = {'error_description': 'Invalid device key!'}
                        return Response(content, status=status.HTTP_403_FORBIDDEN)
                    # continue with the claim process
                    device.user = request.user
                    device.device_key = device_key
                    device.last_seen = datetime.now(timezone.utc)
                    device.is_claimed = True
                    #device name is required
                    if 'device_name' in request.data:
                        device.name = request.data['device_name']
                    else:
                        content = {'error_description': 'Device name is required'}
                        return Response(content, status=status.HTTP_403_FORBIDDEN)
                    # set device pass word MQTT channel
                    try:
                        actioninstance = actions.Actions()
                        if not actioninstance.set_mqqt_creds(device.id, device.device_key):
                            print('setting up MQTT channel failed')
                        actioninstance.disconnect()
                    except Exception as e:
                        print("Error" + str(e))
                        content = {'error_description': 'MQTT connection refused, please file a support ticket'}
                        return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                    else:
                        device.save()
                        serializer = DeviceSerializer(device)
                        return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    content = {'error_description': 'Device is already assigned to another user'}
                    return Response(content, status=status.HTTP_403_FORBIDDEN)

        else:
                    content = {'error_description': 'Device with provided serial number does not exist'}
                    return Response(content, status=status.HTTP_403_FORBIDDEN)

    #@list_route(methods=['get'])
    @action(detail=False, methods=['get'], url_path='heartbeat')
    def heartbeat(self, request):
        print(request.data)
        print('test')
        queryset = Device.objects.filter(serial_number=request.data['serial_number'])
        print(queryset)
        if queryset.exists():
            for device in queryset:
                # self.check_object_permissions(request, device)
                # if request.data['device_key'] != device.device_key:
                #     print(request.data['device_key'])
                #     print('invalid device key')
                #     content = {'invalid device key'}
                #     return Response(content, status=status.HTTP_403_FORBIDDEN)
                # Only allow authentoicated user's to update their own device info
                # Notify friends if the IP has changed
                print("device ip address is " + device.ip_address)
                print("heartbeat ip address is " + request.data['ip_address'])
                notify_ip_change.apply_async(args=(device.id,request.data['ip_address']))
                # email user when the device is back online
                time_diff = datetime.now(timezone.utc)-device.last_seen
                print("Time lapsed since last heartbeat is ", time_diff)
                notify_user_online.apply_async(args=(device.id,))
                # Update device values
                device.port = request.data.get('port')
                device.last_seen = datetime.now(timezone.utc)
                device.status = request.data.get('status')
                device.pin = request.data.get('pin')
                device.local_token = request.data.get('local_token')
                device.local_ip_address = request.data.get('local_ip_address')
                device.software_version = request.data.get('software_version')
                # this part checks if certs are in sync on device and server
                if 'access_cred' in request.data:
                    credentials = request.data['access_cred']
                    # synchronize cerd IDs on device and backend
                    sync_credentials.apply_async(args=(device.id,credentials))
                if 'usage_status' in request.data:
                    usage_status = request.data['usage_status']
                    update_usage_status.apply_async(args=(device.id,usage_status))
                # set flags according to the diagcode
                diag_code = int(request.data.get('diag_code'))
                if (diag_code is not None) & (-1 < diag_code < 128):
                    device.diag_code = diag_code
                else:
                    print('invalid diagcode')
                    content = {'include a valid diag_code between 0 and 127'}
                    return Response(content, status=status.HTTP_403_FORBIDDEN)
                device.save()
                content = {'Successful'}
                return Response(content, status=status.HTTP_200_OK)
        else:
                print('invalid serial number')
                content = {'invalid serial number'}
                return Response(content, status=status.HTTP_403_FORBIDDEN)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='unclaim')
    def unclaim(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print(device.user)
        wipe_friends(device.id)
        time.sleep(2)
        wipe_device(device.id)
        content = {'Successful': 'Device unclaim process started'}
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='reboot')
    def reboot(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print("reboot device")
        content = {'Done': 'Device is rebooting'}
        actioninstance = actions.Actions()
        msg = {'action': 'reboot_device'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='start')
    def start(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print("start service")
        content = {'Done': 'Service is starting'}
        actioninstance = actions.Actions()
        msg = {'action': 'start_service'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='restart')
    def restart(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print("restart service")
        content = {'Done': 'Service is restarting'}
        actioninstance = actions.Actions()
        msg = {'action': 'restart_service'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='reload')
    def reload(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print("reload service")
        content = {'Done': 'Service is reloading'}
        actioninstance = actions.Actions()
        msg = {'action': 'reload_service'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        return Response(content, status=status.HTTP_200_OK)

    #@detail_route(methods=['get'])
    @action(detail=True, methods=['get'], url_path='stop')
    def stop(self, request, pk=None):
        device = self.get_object()
        self.check_object_permissions(request, device)
        print("stop service")
        content = {'Done': 'Service is stopped'}
        actioninstance = actions.Actions()
        msg = {'action': 'stop_service'}
        actioninstance.publish_mqtt_msg(device.id, msg)
        actioninstance.disconnect()
        return Response(content, status=status.HTTP_200_OK)

    def checksum(self, device_key):
        space = string.digits + string.ascii_uppercase
        sum = 0
        for i in device_key:
            try:
                sum = space.index(i) + sum
            except ValueError as valerr:
                print(valerr)
                raise
        return space[sum % len(space)]


#        recent_users = User.objects.all().order('-last_login')
#
#        page = self.paginate_queryset(recent_users)
#        if page is not None:
#            serializer = self.get_serializer(page, many=True)
#            return self.get_paginated_response(serializer.data)
#
#        serializer = self.get_serializer(recent_users, many=True)
#        return Response(serializer.data)

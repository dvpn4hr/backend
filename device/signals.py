from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver
from .models import Device
import random, string
from datetime import datetime, timezone, timedelta

@receiver(pre_save, sender=Device)
def update_status_codes(sender, instance, **kwargs):
    #update status code flags based on diagcode
    diag_code = instance.diag_code
    if diag_code:
        instance.local_network_connected = bool(diag_code & (1 << 0))
        instance.internet_connected = bool(diag_code & (1 << 1))
        instance.services_running = bool(diag_code & (1 << 2))
        instance.port_opened = bool(diag_code & (1 << 3))
        instance.MQTT_connected = bool(diag_code & (1 << 4))
        instance.VPN_selftest = bool(diag_code & (1 << 5))
        instance.is_claimed = bool(diag_code & (1 << 6))

@receiver(pre_save, sender=Device)
def reset_device_params(sender, instance, **kwargs):
    # If instance is NOT being created for the first time
    if instance.id is not None:
        # if the instance being update, reset device params
        # if user field is being set to None
        current = instance
        previous = Device.objects.get(id=instance.id)
        if (previous.user is not None) and (current.user is None):
            #reser device parameters
            name = ''.join(random.choice(string.digits) for _ in range(5))
            current.name = "UnclaimedDevice" + name
            current.port = '0'
            current.last_seen = datetime.now(timezone.utc)
            current.status = 0
            current.pin = '0'
            current.local_token = 'abc'
            current.ip_address = '0.0.0.0'
            current.local_ip_address = '0.0.0.0'
            current.diag_code = 0
            current.owner_notified_offline = True
            current.permission_to_notify_owner = True
            current.is_claimed = False
        if (previous.user is None) and (current.user is not None):
            #reset some device parameters
            current.port = '0'
            current.last_seen = datetime.now(timezone.utc)
            current.status = 0
            current.pin = '0'
            current.local_token = 'abc'
            current.ip_address = '0.0.0.0'
            current.local_ip_address = '0.0.0.0'
            current.diag_code = 0
            current.owner_notified_offline = True
            current.permission_to_notify_owner = True

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models


#from django.apps import apps
#Device = apps.get_model('device','Device')
# Create your models here.

class UserManager(models.Manager):
    def create_user(self, email, firstname, lastname, password):
        """Creates a new user with provided email and password"""
        user = User.objects.create(email=email.lower())
        # print(password)
        user.set_password(password)
        user.firstname = firstname
        user.lastname = lastname
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        return self.get(**{self.model.USERNAME_FIELD + '__iexact': username})

    def create_superuser(self, email, password, firstname='', lastname=''):
        user = self.create_user(email=email,
                                password=password,
                                firstname=firstname,
                                lastname=lastname)
        user.is_admin = True
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class LowercaseEmailField(models.EmailField):
    """
    Override EmailField to convert emails to lowercase before saving.
    """
    def to_python(self, value):
        """
        Convert email to lowercase.
        """
        value = super(LowercaseEmailField, self).to_python(value)
        # Value can be None so check that it's a string before lowercasing.
        if isinstance(value, str):
            return value.lower()
        return value

class User(
    AbstractBaseUser,
    PermissionsMixin
):
    USERNAME_FIELD = 'email'
    id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=200, null=True, blank=True)
    lastname = models.CharField(max_length=200, null=True, blank=True)
    email = LowercaseEmailField(max_length=255,
                                unique=True,
                                error_messages={
                                "unique":"Something went wrong."
                                })
    is_active = models.BooleanField(default=True)
    is_registered = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    introduced = models.BooleanField(default=False)
    date_joined = models.DateTimeField(
        auto_now_add=True,
        editable=True,
        null=False,
        blank=False,
    )
    one_time_password_creation_date = models.DateTimeField(null=True, blank=True)
    one_time_password = models.CharField(max_length=512, null=True, blank=True)
    salt = models.CharField(max_length=512, null=True, blank=True)
    last_activity = models.DateTimeField(null=True, blank=True)

    objects = UserManager()

    def get_short_name(self):
        return self.firstname

    def __str__(self):
        return self.email

    @property
    def full_name(self):
        "Returns the person's full name."
        return '%s %s' % (self.firstname, self.lastname)

# class UserProfile(models.Model):
#     user = models.OneToOneField(User, related_name='profile')
#     firstname = models.CharField(max_length=200, null=False, blank=False)
#     lastname = models.CharField(max_length=200, null=False, blank=False)
#
#     objects = UserManager()

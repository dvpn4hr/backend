from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver
from device.models import Device
from .models import User
from actions import actions

# this method will unclaim device upon deletion of user account
@receiver(post_delete, sender=User)
def unclaim_device(sender, instance, **kwargs):
    queryset = Device.objects.filter(user=instance)
    print('sending MQTT command to wipe device...')
    try:
        if queryset.exists():
            actioninstance = actions.Actions()
            for device in queryset:
                # send mqtt commpand to wipe off info from device
                msg = {'action': 'wipe_device'}
                actioninstance.publish_mqtt_msg(device.id, msg)
                # change MQTT password on the server
                N = 20
                random_key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N))
                if not actioninstance.set_mqqt_creds(device.id, random_key):
                   print('changing MQTT password failed for device ' + "device.id")
            actioninstance.disconnect()
        else:
            print("No devices found for user = " + str(instance.email))
    except Exception as e:
        print(" Error" + str(e))
        raise e

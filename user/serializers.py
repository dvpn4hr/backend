from django.contrib.auth import get_user_model
from rest_framework import serializers
import django.contrib.auth.password_validation as validators
from django.core import exceptions


from .models import User


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True, read_only=True)
    password = serializers.CharField(write_only=True)
    firstname = serializers.CharField(required=False, allow_null=True, default=None)
    lastname = serializers.CharField(required=False, allow_null=True, default=None)

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'password',
            'firstname',
            'lastname',
        )

    def validate(self, data):
        # validators.validate_password(password=data, user=User)
        # return data

        # here data has all the fields which have validated values
        # so we can create a User instance out of it
        if data.get('password'):
            print(data)
            user = User(**data)

            # get the password from the data
            password = data.get('password')

            errors = dict()
            try:
                # validate the password and catch the exception
                validators.validate_password(password=password, user=user)

            # the exception raised here is different than serializers.ValidationError
            except exceptions.ValidationError as e:
                errors['password'] = list(e.messages)

            if errors:
                raise serializers.ValidationError(errors)

        return super(UserSerializer, self).validate(data)

    def create(self, validated_data):
        return User.objects.create_user(
            password=validated_data['password'],
            email=validated_data['email'],
            firstname=validated_data['firstname'],
            lastname=validated_data['lastname'],
        )

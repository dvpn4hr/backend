from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets, status
from django.db import IntegrityError
#from django.utils.decorators import method_decorator
from .email import send_email
import random
import string
from datetime import datetime, timezone
#from ratelimit.decorators import ratelimit

from .models import User
from .serializers import UserSerializer
from .permissions import IsSameOrAdmin
import hashlib, uuid

#@method_decorator(ratelimit(key='ip', rate='5/m'), name='forgot_password')
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of
        permissions that this view requires.
        """
        print(self.action)
        if self.action in ['list', 'retrieve', 'update', 'partial_update', 'destroy']:
            # Only allow same user to view/edit/update its own record
            print('execution reaches here')
            permission_classes = [ IsSameOrAdmin, IsAuthenticated]
        elif self.action in ['create', 'forgot_password', 'reset_password']:
            # allow anyone to create (sign up) a new user
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def list(self, request):
        print('execution reaches list body')
        queryset = User.objects.filter(email=request.user.email.lower())
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    #@list_route(methods=['post'])
    #@ratelimit(key='ip', rate='5/m')
    @action(detail=False, methods=['post'], url_path='forgot_password')
    def forgot_password(self, request):
        print(request.data)
        email_address = request.data['email'].lower()
        print(email_address)
        queryset = User.objects.filter(email=email_address)
        print(queryset)
        if queryset.exists():
            print("Generating one-time password")
            for user in queryset:
                N = 10  # length of one time password
                otp = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N))
                plain_password = otp.encode('utf-8')
                salt = uuid.uuid4().hex
                hashed_password = hashlib.sha512(plain_password + salt.encode('utf-8')).hexdigest()
                user.one_time_password = hashed_password
                user.one_time_password_creation_date = datetime.now()
                user.salt = salt
                user.save()
                print("Sending email address")
                send_email(user.email, otp)
            # send email to user with one time password
        content = {'Email sent to user with one-time password!'}
        return Response(content, status=status.HTTP_200_OK)


    #@list_route(methods=['post'])
    @action(detail=False, methods=['post'], url_path='reset_password')
    def reset_password(self, request):
        print(request.data)
        queryset = User.objects.filter(email=request.data['email'].lower())
        print(queryset)
        if queryset.exists():
            print("checking expiration date and one time password match")
            plain_password = request.data['one_time_password'].encode('utf-8')
            for user in queryset:
                hashed_password = hashlib.sha512(plain_password + user.salt.encode('utf-8')).hexdigest()
                if user.one_time_password and (user.one_time_password == hashed_password):
                    expiration = datetime.now(timezone.utc) - user.one_time_password_creation_date
                    print('expiration days is' + str(expiration.days))
                    if (expiration.seconds < 3600):
                        user.set_password(request.data['new_password'])
                        user.one_time_password = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(20))
                        user.save()
                        content = {'Password successfully changed!'}
                        return Response(content, status=status.HTTP_200_OK)
                    else:
                        content = {'error_description':'One-time password is expired!'}
                        return Response(content, status=status.HTTP_403_FORBIDDEN)
                else:
                        print('invalid one-time password')
                        #content = {'error_description':'invalid one-time password!'}
                        content = {'error_description':'Something went wrong'}
                        return Response(content, status=status.HTTP_403_FORBIDDEN)
        else:
            print('No match found with username - email')
            content = {'error_description':'Something went wrong!'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

    # def list(self, request):
    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except IntegrityError:
            content = {'error': 'You cannot perform this action'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)
    # def retrieve(self, request, pk=None):
    # def update(self, request, pk=None):
    # def partial_update(self, request, pk=None):
    # def destroy(self, request, pk=None):

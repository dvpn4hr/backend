from rest_framework.permissions import BasePermission
from .models import User


class IsSameOrAdmin(BasePermission):
    """Custom permission class to allow only owners to edit them."""
    def has_object_permission(self, request, view, obj):
        """Return True if permission is granted to the bucketlist owner."""
        if isinstance(obj, User):
            #print("request.data.email = " + str(request.data.get('email')))
            #print("request.user.email = " + str(request.user.email))
            #print("obj.email = " + obj.email)
            #print("obj.id = " + str(obj.id))
            #print("request.user.id = " + str(request.user.id))
            #print("request.id = " + str(request.data.get('id')))
            if request.data.get('id'):
                sameID = (str(obj.id) == str(request.user.id)) \
                    and (str(obj.id) == str(request.data.get('id')))
            else:
                sameID = True
            sameEmail = (obj.email == request.user.email)
            #print('sameID is ' + str(sameID) )
            #print('sameEmail is ' + str(sameEmail) )
            return ((sameID and sameEmail) or request.user.is_staff)
        # print("request.user.email = " + request.user.email)
        # print("obj.email = " + obj.email)
        #return ((obj.email == request.user.email) | request.user.is_staff)
        else:
            return False

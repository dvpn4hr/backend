from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from rest_framework import routers
from django_otp.admin import OTPAdminSite
from django.contrib.auth.models import User
from django_otp.plugins.otp_totp.models import TOTPDevice
from django_otp.plugins.otp_totp.admin import TOTPDeviceAdmin
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView


import user.api
import device.api
import friend.api
import experiment.api
import message.api
import user.views
router = routers.DefaultRouter()
router.register(r'user', user.api.UserViewSet)
router.register(r'device', device.api.DeviceViewSet)
router.register(r'friend', friend.api.FriendViewSet)
router.register(r'experiment', experiment.api.ExperimentViewSet)
router.register(r'message', message.api.MessageViewSet)

# url(r'^api/', include(router.urls)),

admin.site.__class__ = OTPAdminSite
admin.autodiscover()
admin.site.enable_nav_sidebar = False

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api-auth/', include('rest_framework.urls',
        namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    # Optional UI:
    #path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('api/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]

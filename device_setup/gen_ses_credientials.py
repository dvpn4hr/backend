import boto3
from gen_smtp_password import gen_password


def create_ses_email(user_name):
    # set static values
    group_name = 'rpi-devices'
    # user_name = 'deviceXX'

    iam = boto3.resource('iam')
    user = iam.User(user_name)

    # create IAM user
    user = user.create()
    print(user)

    # generate access key for user
    access_key_pair = user.create_access_key_pair()
    # print(access_key_pair)
    secret_access_key = access_key_pair.secret_access_key
    ses_username = access_key_pair.access_key_id
    print("access secret key is " + secret_access_key)

    response = user.add_group(
        GroupName=group_name,
    )
    print(response)

    ses_password = gen_password(secret_access_key)
    print(ses_password)
    return [ses_username, ses_password]

# import os      # required to fetch environment varibles
import hmac    # required to compute the HMAC key
import hashlib  # required to create a SHA256 hash
import base64  # required to encode the computed key
import sys     # required for system functions (exiting, in this case)


def gen_password(key):
    # Fetch the environment variable called AWS_SECRET_ACCESS_KEY,
    # which contains the secret access key for your IAM user.
    # key = os.getenv('AWS_SECRET_ACCESS_KEY', 0)

    # These varibles are used when calculating the SMTP password. You shouldn't
    # change them.
    message = 'SendRawEmail'
    version = '\x02'

    # See if the environment variable exists. If not, quit and show an error.
    if key == 0:
        sys.exit("Error: Can't find environment\
                    variable AWS_SECRET_ACCESS_KEY.")

    # Compute an HMAC-SHA256 key from the AWS secret access key.
    signatureInBytes = hmac.new(
                        key.encode('utf-8'),
                        message.encode('utf-8'),
                        hashlib.sha256
                        ).digest()
    # Prepend the version number to the signature.
    signatureAndVersion = version.encode('utf-8') + signatureInBytes
    # Base64-encode the string that contains the version number and signature.
    smtpPassword = base64.b64encode(signatureAndVersion)
    # Decode the string and print it to the console.
    print(smtpPassword.decode('utf-8'))
    return smtpPassword.decode('utf-8')

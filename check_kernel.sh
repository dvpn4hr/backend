#!/bin/bash
# email me whenever kernel needs updating
# based on script from https://tehtable.wordpress.com/2010/05/19/another-magic-sysadmin-script/
#
EMAIL_SUBJECT="New kernel available"
FROM_EMAIL_ADDRESS=devices@we-pn.com
FRIENDLY_NAME="WEPN Server"
TO_EMAIL_ADDRESS=manage@we-pn.com
 
ksplice_str=`(uname -s; uname -m; uname -r; uname -v) |  curl -s https://api-ksplice.oracle.com/api/1/update-list/ -L -H "Accept: text/text" --data-binary @-`
 
if [ "$ksplice_str" != "Your kernel is up to date." ]
then
	ssmtp $TO_EMAIL_ADDRESS -f $FROM_EMAIL_ADDRESS -F "WEPN Server" << EOF
To: Admin <$TO_EMAIL_ADDRESS>
From: $FRIENDLY_NAME <$FROM_EMAIL_ADDRESS>
Subject: $EMAIL_SUBJECT

Hi Admin,
 
WEPN server needs an update, KSPLICE tells me this:
$ksplice_str

Please check it. 



EOF
else
     echo -e "All is well, KSPLICE says: \n $ksplice_str"
fi

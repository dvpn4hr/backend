#!/usr/bin/python
import json
import ssl
import paho.mqtt.client as mqtt
from time import sleep
#import pp_backend.local_settings as settings
import subprocess
import shlex
from decouple import AutoConfig
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_PATH = os.path.join(BASE_DIR, 'pp_backend/')
config = AutoConfig(search_path=CONFIG_PATH)

class Actions:
	def __init__(self):
		self.client = mqtt.Client(config('mqtt_clientname'))
		self.client.tls_set(config('mqtt_tls_cert'), tls_version=ssl.PROTOCOL_TLSv1_2)
		self.client.username_pw_set(config('mqtt_username'), config('mqtt_password'))
		self.client.connect(config('mqtt_host'), config('mqtt_port', cast=int), 900)
		# TODO: Move all hardcoded values to local setting file
		self.client.loop_start()
		return

	def publish_mqtt_command(self, device, cmd, cert_name, email, passcode, language='en'):
		send_msg = {
			'action': cmd,
			'cert_name': cert_name,
			'email': email,
			'passcode': passcode,
			'language': language
		}
		topic = "devices/"+str(device)
		publish_info=self.client.publish(topic, payload=json.dumps(send_msg), qos=2)
		while(publish_info.rc!=mqtt.MQTT_ERR_SUCCESS):
			if(publish_info.rc==mqtt.MQTT_ERR_NO_CONN):
				print("Cannot publish, MQTT_ERR_NO_CONN")
				self.client.reconnect()
				publish_info=self.client.publish(topic, payload=json.dumps(send_msg), qos=2)
			if(publish_info.rc==mqtt.MQTT_ERR_QUEUE_SIZE):
				print("Cannot publish, MQTT_ERR_QUEUE_SIZE")
		#rc2=publish_info.wait_for_publish()
		print(json.dumps(send_msg)+"\n Publishing command using MQTT> " + cmd + ","+cert_name + ","+email+"to "+topic+" result:"+str(publish_info.rc))
		sleep(1)

	def set_mqqt_creds(self, device, password):
		passwordfile = '/var/lib/mosquitto/pwd.db'
		cmd = 'sudo /usr/local/sbin/add_mqtt.sh ' + str(device) + ' ' + password
		r = self.execute_cmd(cmd)
		return r

	def publish_mqtt_msg(self, device, msg, qos_level=2):
		num_retries_left = 5
		topic = "devices/"+str(device)
		publish_info = self.client.publish(topic,
						  				   payload=json.dumps(msg),
										   qos=qos_level)
		while((publish_info.rc != mqtt.MQTT_ERR_SUCCESS) and num_retries_left>0):
			if(publish_info.rc==mqtt.MQTT_ERR_NO_CONN):
				print("Cannot publish, MQTT_ERR_NO_CONN")
				self.client.reconnect()
				publish_info=self.client.publish(topic,
												 payload=json.dumps(msg),
												 qos=qos_level)
			if(publish_info.rc==mqtt.MQTT_ERR_QUEUE_SIZE):
				print("Cannot publish, MQTT_ERR_QUEUE_SIZE")
			print(publish_info.rc)
			num_retries_left -= 1
			sleep(1)
		print(json.dumps(msg))
		sleep(1)

	def disconnect(self):
		self.client.disconnect()
		self.client.loop_stop()
		print("MQTT client disconnecting...")

	def execute_cmd(self, cmd):
		try:
			args = shlex.split(cmd)
			process = subprocess.Popen(args)
			process.wait()
			return True
		except Exception as error_exception:
			print(args)
			print("Error happened in running command:" + cmd)
			print("Error details:\n"+str(error_exception))
			process.kill()
			return False

from django.contrib import admin
from .models import Message

# Register your models here.

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'device', 'creation_datetime','read_datetime','expiary_datetime', 'message_body', 'message_response', 'is_read')
    search_fields = ['device__user__email']

# Register your models here.
admin.site.register(Message, MessageAdmin)

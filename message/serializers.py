from rest_framework import serializers
from datetime import datetime
from .models import Message


class MessageSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True, read_only=True)
    message_body = serializers.JSONField(required=True)
    # message_response = serializers.JSONField(required=False)
    # creation_datetime = serializers.DateTimeField(required=False, allow_null=True)
    # read_datetime = serializers.DateTimeField(required=False, allow_null=True)
    # expiary_datetime = serializers.DateTimeField(required=False, allow_null=True)
    # add these two lines but pop these field when creating instances
    serial_number = serializers.CharField(write_only=True)
    device_key = serializers.CharField(write_only=True)
    destination = serializers.CharField(required=True)

    class Meta:
        model = Message
        fields = (
            'serial_number',
            'device_key',
            'id',
            'message_body',
            'message_response',
            'is_read',
            'is_expired',
            'destination',
        )

    def create(self, validated_data):
        print(validated_data)
        # validated_data.pop('serial_number', None)
        # validated_data.pop('device_key', None)
        # new messages must have at least these three fields
        return Message.objects.create(
                        device = validated_data['device'],
                        message_body = validated_data['message_body'],
                        destination = validated_data['destination'],
        )


from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, action
from django.shortcuts import get_object_or_404

from device.models import Device
from user.models import User
from .models import Message
from .serializers import MessageSerializer
from .permissions import IsValidDevice
from actions import actions


import pprint
import logging
import random
import string
import ipaddress
from datetime import datetime, timezone, timedelta


# Get an instance of a logger
# logger = logging.getLogger(__name__)


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of
        permissions that this view requires.
        """
        print(self.action)
        # This makes sure request contains
        # valid device credential

        if self.action in ['list', 'retrieve', 'update','partial_update', 'create']:
            permission_classes = [IsValidDevice]
        #     # Only allow same user to view/edit/update its own record
        #     print('execution reaches here')
        #     permission_classes = [IsAuthenticated] #, IsSame]
        # elif self.action in ['create', 'result']:
        #     # allow anyone to create (sign up) a new user
        #     permission_classes = [AllowAny]
        elif self.action == 'destroy':
             permission_classes = [IsAdminUser]
        else:
             permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]


    def create(self, request, *args, **kwargs):
        """
        ### Purpose 
        POST call to this API creates a new message. <br>
        ### Authentication
        The payload must include `serial_number` and `device_key` for authentication <br>
        Authentication headers for Oauth teken is ignored. <br>
        The message can be initiated from the device, the mobile app clients, or backend <br> 
        """
        # POST call to /api/message will create a new message record
        # print("creating the serializer")
        print(request.data)
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        print(request.data.get("message_body"))
        # I think we should
        serializer = self.get_serializer(data=request.data)
        # serializer = self.get_serializer(data=
        #            {'message_body': request.data.get("message_body"),
        #            'destination':request.data.get("destination")
        #            })
        # serializer.user = request.user
        try:
            serializer.is_valid(raise_exception=True)
            # print(request.data)
            queryset = Device.objects.filter(serial_number=serial_number,
                                            device_key=device_key)
            if queryset.exists():
                for device in queryset:
                    serializer.save(device=device)
                    print(serializer.data)
                    #start async celery task to process the message
                    #start_experiment.apply_async(args=(serializer.data['id'],), countdown=2)
                    headers = self.get_success_headers(serializer.data)
                    return Response(
                                    serializer.data,
                                    status=status.HTTP_201_CREATED,
                                    headers=headers
                                    )
            else:
               content = {'error': 'request denied'}
               return Response(content, status=status.HTTP_403_FORBIDDEN)
            # try saving the serializer ang go to else if everything goes well
        except Exception as e:
            print(" Error:" + str(e))
            content = {'error_description': 'invalid data'}
            return Response(content, status=status.HTTP_409_CONFLICT)


    def list(self, request):
        # GET /message/
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        is_read = request.data.get("is_read")
        is_expired = request.data.get("is_expired")
        destination = request.data.get("destination")
        queryset = Device.objects.filter(serial_number=serial_number, device_key=device_key)
        # allow GET to include paraeters such as is_read, creation_time, expiray
        print(request.data)
        if queryset.exists():
            for device in queryset:
                #apply more filters here
                msg_qset = Message.objects.filter(device=device)
                # if request includes the following fields, then create
                # a subset query and narrow down the results
                if is_read is not None:
                    msg_qset = msg_qset.filter(is_read=is_read)
                if is_expired is not None:
                    msg_qset = msg_qset.filter(is_expired=is_expired)
                if destination is not None:
                    msg_qset = msg_qset.filter(destination=destination)
                serializer = MessageSerializer(msg_qset, many=True)
                return Response(serializer.data)
        else:
            content = {'error': 'request denied'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)


    def retrieve(self, request, pk=None):
        # GET /message/pk
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        queryset = Message.objects.all()
        message = get_object_or_404(queryset, pk=pk)
        # check if message is associate with device with correct credentials
        # only allow retrieving messages associated with given device credentials
        if ((message.device.device_key == device_key) and
            (message.device.serial_number ==serial_number)):
            serializer = MessageSerializer(message)
            return Response(serializer.data)
        else:
            content = {'error': 'request denied'}
            return Response(content, status=status.HTTP_403_FORBIDDEN)

    #update
    # PUT /message/pk
    # def update(self, request, pk=None):
    #     # GET /message/pk
    #     serial_number = request.data.get("serial_number")
    #     device_key = request.data.get("device_key")
    #     queryset = Message.objects.all()
    #     message = get_object_or_404(queryset, pk=pk)
    #     # check if message is associate with device with correct credentials
    #     # only allow retrieving messages associated with given device credentials
    #     if ((message.device.device_key == device_key) and
    #         (message.device.serial_number ==serial_number)):
    #         serializer = self.get_serializer(request.data)
    #         try:
    #             serializer = MessageSerializer(request)
    #             serializer.is_valid(raise_exception=True)
    #             serializer.save()
    #             print(serializer.data)
    #             #start async celery task to process the message
    #             #start_experiment.apply_async(args=(serializer.data['id'],), countdown=2)
    #             #send MQTT action to device to notify receipt of message
    #             headers = self.get_success_headers(serializer.data)
    #             return Response(
    #                             serializer.data,
    #                             status=status.HTTP_201_CREATED,
    #                             headers=headers
    #                             )
    #
    #         return Response(serializer.data)
    #     else:
    #         content = {'error': 'request denied'}
    #         return Response(content, status=status.HTTP_403_FORBIDDEN)

    #partial_update
    # UPDATE /message/pk

    # @detail_route(methods=['post'])
    # def result(self, request, pk=None):
    #     try:
    #         message = self.get_object()
    #         serial_number = request.data.get('serial_number')
    #         print(serial_number)
    #         device_key = request.data.get('device_key')
    #         print(device_key)
    #         print("message's device serial is", message.device.serial_number)
    #         if (message.device.serial_number == serial_number) and \
    #            (message.device.device_key == device_key):
    #            serializer = MessageSerializer(message)
    #            return Response(serializer.data, status=status.HTTP_200_OK)
    #         else:
    #            content = {'error': 'request denied'}
    #            return Response(content, status=status.HTTP_403_FORBIDDEN)
    #     except Exception as e:
    #         print("Error: " + str(e))
    #         content = {'error_description': 'something went wrong'}
    #         return Response(content, status=status.HTTP_409_CONFLICT)

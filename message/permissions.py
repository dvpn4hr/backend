from rest_framework.permissions import BasePermission
from device.models import Device
from user.models import User


class IsValidDevice(BasePermission):
    """Only allow API calls with valid serial number and device key."""

    def has_permission(self, request, view):
        """Return True if it includes valid device credential."""
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        print(serial_number)
        print(device_key)
        queryset = Device.objects.filter(serial_number=serial_number,
                                        device_key=device_key)
        if queryset.exists():
            print("authentication passed")
            return True
        else:
            print("authentication failed")
            return False
        # print("request.user.email = " + request.user.email)
        # print("obj.email = " + obj.email)
        #return obj.user == request.user
    def has_object_permission(self, request, view, obj):
        """Return True if device credentials match with obj"""
        serial_number = request.data.get("serial_number")
        device_key = request.data.get("device_key")
        print(serial_number)
        print(device_key)
        queryset = Device.objects.filter(serial_number=serial_number,
                                        device_key=device_key)
        if queryset.exists():
            for device in queryset:
                if ((serial_number == obj.device.serial_number) and
                        (device_key == obj.device.device_key)):
                    return True
                else:
                    print("authentication failed at object level")
                    return False
        else:
            print("authentication failed - no device with such credential")
            return False

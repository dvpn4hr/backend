from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

# Create your models here.
from device.models import Device
from user.models import User
from actions import actions
from django.utils import timezone
from datetime import datetime, timezone, timedelta


class Message(models.Model):
    class Destination(models.TextChoices):
            APP = 'APP'
            DEVICE = 'DEVICE'
            BACKEND = 'BACKEND'
            ALL = 'ALL'

    id = models.AutoField(primary_key=True)
    device = models.ForeignKey(Device, related_name='message_device', on_delete=models.SET_NULL, null=True, blank=True)
    creation_datetime = models.DateTimeField('created on', null=True, blank=True)
    read_datetime = models.DateTimeField('read on', null=True, blank=True)
    expiary_datetime = models.DateTimeField('expired on', null=True, blank=True)
    is_read = models.BooleanField(default=False, blank=True)
    is_expired = models.BooleanField(default=False, blank=True)
    message_body = models.JSONField(null=True, blank=False)
    message_response = models.JSONField(null=True, blank=True)
    destination = models.CharField(
       max_length=7,
       choices=Destination.choices,
       default=Destination.ALL,
       blank=False
    )

    def __str__(self):
        return str(self.id)


@receiver(pre_save, sender=Message)
def update_fields(sender, instance, **kwargs):
    if instance.id is None:
        instance.creation_datetime=datetime.now(timezone.utc)
    if ((instance.is_read) and (instance.read_datetime is None)):
        instance.read_datetime=datetime.now(timezone.utc)

        #send MQTT action to device to notify receipt of message
        #actioninstance = actions.Actions()
        # msq = {'action': 'notification'}
        #actioninstance.publish_mqtt_command(device.id, msg)
        #actioninstance.disconnect()

@receiver(post_save, sender=Message)
def notify_device_of_message(sender, instance, created, **kwargs):
    if created:
        print(str(instance.destination))
        if (str(instance.destination) == 'DEVICE'):
            print("sending mqtt command to notify device of a new message")
            try:
                actioninstance = actions.Actions()
                print("sending MQTT command to device " + str(instance.device.id))
                msg = {'action': 'notification'}
                actioninstance.publish_mqtt_msg(instance.device.id, msg)
                actioninstance.disconnect()
            except Exception as e:
                print(" Error" + str(e))
                raise e
